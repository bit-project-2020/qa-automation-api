FROM node:dubnium-alpine

WORKDIR /usr/src/app

ENV PATH /app/node_modules/.bin:$PATH


COPY . ./

RUN npm install


EXPOSE 7777

CMD npm run build && node build/index.js

