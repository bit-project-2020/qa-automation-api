import dotenv from 'dotenv'
dotenv.config()
import express from 'express';

import cookieParser from 'cookie-parser';

import createServer from './create-server';

// import cors from 'cors';

const PORT = process.env.PORT;

import postgresPool from './db';

const startServer = async() => {
  try{
    const postgresConnection = await postgresPool.connect();
    await postgresConnection.query('ALTER DATABASE postgres SET search_path TO default');

  } catch( error) {
    console.log(error);
  }
  const server = await createServer(postgresConnection);

  server.express.use(cookieParser());

  server.start(
    {
      port: PORT,
      cors: 
       {
        credentials: true,
        origin: [process.env.ADMIN_APP_URL, process.env.USER_APP_URL, /\.candydreams\.site$/,"http://localhost"]
        //origin: true
      }
    },
    ({ port }) => {
      console.log(`Server is now running on port http://localhost:${port}`);
    },
    
  );
};

startServer();

