import { GraphQLServer } from 'graphql-yoga';
import Mutation from './resolvers/Mutation';
import Query from './resolvers/Query';

import permissions from './permissions';
import getUser from './lib/getUser';
// import postgresPool from './db';

async function createServer(postgresPool) {
  // postgresPool.connect();
  const gqlServer =  new GraphQLServer({
    typeDefs: 'src/schema.graphql',
    resolvers: {
      Mutation,
      Query,
    },
    // resolverValidationOptions: {
    //   requireResolversForResolveType: false,
    // },
    
    middlewares: [permissions],
    context: async (req,res) => ({ ...req, postgresPool,
    user: await getUser({ postgresPool, req }),
    }),
  });
  return gqlServer;
}

export default createServer;
