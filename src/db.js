import { Pool } from 'pg';

const pool = new Pool({
  host: process.env.POSTGRES_HOST,
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: 'postgres',
  port: process.env.POSTGRES_PORT
});


pool.on('connect', () => { console.log('postgres pool connected') });

export default pool;
