const inspectionData = async (parent, args, ctx, info) => {
  try {
    const { plant, type, product, size, date_gt, date_lt } = args.where;
    const pool = ctx.postgresPool;

    // get the table we want to fetch the data
    const table = `${plant}_${type}`.toLowerCase();

    // to do serious filtering options

    const whereBlock = `product='${product}' AND size='${size}'`;

    if (date_gt) {
      whereBlock = `${whereBlock} AND date >= '${date_gt}'`;
    }
    if (date_lt) {
      whereBlock = `${whereBlock} AND date <= '${date_lt}'`;
    }

    const fetchDataQuery = `SELECT * FROM ${table} WHERE ${whereBlock} ORDER BY date DESC`;

    const result = await pool.query(fetchDataQuery);

    const plantParametersQuery = `SELECT
    plp.id,
    plp.plant,
    plp.parameter,
    plp.is_active,
    plp.unit,
    parameter_type,
    type
    FROM plant_parameter plp
    INNER JOIN
    parameter ON parameter.name = plp.parameter
    WHERE plant='${plant}' AND type='${type}'`;

    const plantPrametersResult = await pool.query(plantParametersQuery);

    // now prepare the data list
    const output = [];

    result.rows.forEach((row) => {
      // copy originals to the processedObject
      const processedObject = {
        product: row.product,
        size: row.size,
        id: row.id,
        user_id: row.user_id,
        shift: row.shift,
        comment: row.comment,
        date: row.date,
        is_deviated: row.is_deviated,
        extra_fields: [],
      };

      // delete the rest of the parameters from rows object
      const fieldsToremoveFromOriginal = [
        "product",
        "size",
        "id",
        "user_id",
        "comment",
        "date",
        "shift",
      ];
      fieldsToremoveFromOriginal.forEach((e) => delete row[e]);

      Object.keys(row).forEach((key) => {
        const paramFound = plantPrametersResult.rows.find(
          (param_row) => param_row.parameter === key
        );
        if (paramFound) {
          const { parameter_type } = paramFound;
          const subObject = { parameter: key, type: parameter_type };
          if (
            parameter_type === "NUMBER" ||
            parameter_type === "NUMBER_EXACT"
          ) {
            subObject.numeric_value = row[key];
          } else if (parameter_type === "IMAGE" || parameter_type === "TEXT") {
            subObject.text_value = row[key];
          } else if (parameter_type === "BOOLEAN") {
            subObject.boolean_value = row[key];
          } else if (parameter_type === "DATE") {
            subObject.text_value = row[key];
          }

          processedObject.extra_fields.push(subObject);
        }
      });

      output.push(processedObject);
    });

    return output;

    // return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "inspection-data",
        data: error.message,
      })
    );
  }
};

export default inspectionData;
