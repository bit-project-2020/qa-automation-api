import populateSpecsForParams from "../../../lib/populateSpecsForParams";

// have to send the ncr report data with this

const inspectionDataRecord = async (parent, args, ctx, info) => {
  try {
    const { plant, record_type, recordId } = args;
    const pool = ctx.postgresPool;

    // get the table we want to fetch the data
    const table = `${plant}_${record_type}`.toLowerCase();

    // to do serious filtering options
    const fetchDataQuery = `SELECT * FROM ${table} WHERE id=${recordId}`;
    const result = await pool.query(fetchDataQuery);
    const record = result.rows[0];

    console.log(4010120, record);

    // lets get product data
    const productQuery = `SELECT * FROM product WHERE id=${record.product}`;
    const productQueryResult = await pool.query(productQuery);
    const product = productQueryResult.rows[0];

    // lets get size data
    const sizeQuery = `SELECT * FROM size WHERE id=${record.size}`;
    const sizeQueryResult = await pool.query(sizeQuery);
    const size = sizeQueryResult.rows[0];

    // get relavent plant parameters and their specification value
    const plantParametersPopulated = await populateSpecsForParams(
      {
        plant,
        product: record.product,
        size: record.size,
        spec_type: record_type,
      },
      pool
    );

    // copy originals to the processedObject
    const processedObject = {
      product: product,
      size: size,
      id: record.id,
      created_by: record.user_id,
      approved_by: record.approved_by,
      type: record.type,
      shift: record.shift,
      comment: record.comment,
      date: record.date,
      extra_fields: [],
    };

    // delete the rest of the parameters from rows object
    const fieldsToremoveFromOriginal = [
      "product",
      "size",
      "id",
      "user_id",
      "comment",
      "date",
      "shift",
    ];

    fieldsToremoveFromOriginal.forEach((e) => delete record[e]);

    Object.keys(record).forEach((key) => {
      const paramFound = plantParametersPopulated.find(
        (param_row) => param_row.parameter === key
      );
      if (paramFound) {
        const { parameter_type } = paramFound;
        const subObject = { parameter: key, type: parameter_type };
        if (parameter_type === "NUMBER" || parameter_type === "NUMBER_EXACT") {
          subObject.numeric_value = record[key];
        } else if (parameter_type === "IMAGE" || parameter_type === "TEXT") {
          subObject.text_value = record[key];
        } else if (parameter_type === "BOOLEAN") {
          subObject.boolean_value = record[key];
        } else if (parameter_type === "DATE") {
          subObject.text_value = record[key];
        }

        // add specification here
        subObject.specification = paramFound.specification;

        processedObject.extra_fields.push(subObject);
      }
    });

    // console.log(processedObject);

    // get deviations and put it to the processed object
    processedObject.deviations = [];

    const getDeviationsQuery = `SELECT
      p.name,
      p.type,
      plant_parameter.parameter,
      deviation.id,
      deviation.value,
      deviation.specification,
      deviation.created_by,
      deviation.created_at,
      deviation.plant_parameter,
      deviation.record_id,
      deviation.plant,
      deviation.product,
      deviation.size
    FROM
      deviation
    JOIN
      plant_parameter ON plant_parameter.id = deviation.plant_parameter
    JOIN
      parameter p ON plant_parameter.parameter = p.name
    WHERE
      record_id=${recordId} AND deviation.plant='${plant}' AND type='${record_type}'`;

    // console.log(getDeviationsQuery);
    const getDeviationsResult = await pool.query(getDeviationsQuery);
    //  console.log(getDeviationsResult);

    if (getDeviationsResult.rows.length > 0) {
      processedObject.deviations = getDeviationsResult.rows;
    }

    const getNCRQuery = `SELECT
      *
    FROM
      ncr_data
    WHERE
      plant='${plant}' AND record_id='${recordId}' AND type='${record_type}'`; // set the record_id correctly

    // console.log(444444, getNCRQuery);
    const getNCRResult = await pool.query(getNCRQuery);
    // console.log(555555, getNCRResult);

    processedObject.ncr = getNCRResult.rows[0];

    console.log(33333, processedObject);

    return processedObject;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "inspection-data",
        data: error.message,
      })
    );
  }
};

export default inspectionDataRecord;
