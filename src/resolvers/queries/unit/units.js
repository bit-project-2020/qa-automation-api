const units = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    const productsQuery = 'SELECT * FROM "unit"';
    // console.log(args);

    const result = await pool.query(productsQuery);
    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "units",
        data: error.message,
      })
    );
  }
};

export default units;
