const plants = async (parent, args, ctx, info) => {
  try {
    // do the poolings here later
    const plant_name = (args.where && args.where.plant_name) || undefined;
    const pool = ctx.postgresPool;

    // implement filterings and do the job
    let where_block = "";
    if (plant_name != undefined) {
      where_block = ` WHERE plant_name='${plant_name}'`;
    }

    const plantQuery = `SELECT * FROM plant${where_block}`;

    const result = await pool.query(plantQuery);
    return result.rows;
  } catch (error) {
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "current-user",
        data: error.message,
      })
    );
  }
};

export default plants;
