const currentUser = async (parent, args, ctx, info) => {
  try {
    if(!ctx.user){
      // throw new Error('Please login');
      return null;
    }

    return ctx.user;
  } catch (error) {
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'current-user', data: error.message })
    );
  }
};

export default currentUser;
