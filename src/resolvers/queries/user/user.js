const user = async (parent, args, ctx, info) => {
  try {
    // do the poolings here later
    const pool = ctx.postgresPool;

    const userQuery = `SELECT * FROM "user" where id='${args.where.id}'`;

    const result = await pool.query(userQuery);
    return result.rows[0];
    
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'user', data: error.message })
    );
  }
};

export default user;
