const users = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    const usersQuery = 'SELECT * FROM "user"';

    const result = await pool.query(usersQuery);

    return result.rows;

  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'users', data: error.message })
    );
  }
};

export default users;
