const specParameters = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;
    const result = await pool.query('SELECT * FROM parameter');
    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'current-user', data: error.message })
    );
  }
};

export default specParameters;
