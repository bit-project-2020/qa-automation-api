const size = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    const productsQuery = `SELECT * FROM "size" WHERE product='${args.where.product_id}'`;
    console.log(productsQuery);

    const result = await pool.query(productsQuery);
    

    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "sizes",
        data: error.message,
      })
    );
  }
};

export default size;
