const size = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;


    const sizeQuery = `SELECT * FROM "size" where id='${args.where.id}'`;

    const result = await pool.query(sizeQuery);
    return result.rows[0];

  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'size', data: error.message })
    );
  }
};

export default size;
