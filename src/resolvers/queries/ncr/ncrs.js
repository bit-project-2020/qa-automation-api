const ncrs = async (parent, args, ctx, info) => {
  try {
    let filteringBlock = "";
    // console.log(args.where);
    if (args.where && Object.keys(args.where).length !== 0) {
      filteringBlock = "WHERE ";
      const keys = Object.keys(args.where);
      keys.forEach((key) => {
        if (filteringBlock.length > 6) {
          filteringBlock = `${filteringBlock} AND `;
        }
        // handle greaterthan lower than thing
        if (key.includes("_gt")) {
          const keyPortion = key.split("_gt")[0];
          filteringBlock = `${filteringBlock}${keyPortion} >= '${args.where[key]}'::timestamp`;
        } else if (key.includes("_lt")) {
          const keyPortion = key.split("_lt")[0];
          filteringBlock = `${filteringBlock}${keyPortion} <= '${args.where[key]}'::timestamp`;
        } else {
          filteringBlock = `${filteringBlock}${key}='${args.where[key]}'`;
        }
      });
    }

    const pool = ctx.postgresPool;

    const ncrsQuery = `SELECT n.*, s.size_name, p.name as product_name  FROM"ncr_data" n
    JOIN
      "size" s ON s.id =  n.size
    JOIN "product" p
     ON p.id = n.product
    ${filteringBlock}`;
    console.log(ncrsQuery);

    const result = await pool.query(ncrsQuery);

    console.log(result.rows);

    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "ncrs",
        data: error.message,
      })
    );
  }
};

export default ncrs;
