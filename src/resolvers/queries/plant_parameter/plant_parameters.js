const plantParameters = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    let whereSection = "where ";
    // console.log(args);

    const plant = args.where && args.where.plant;
    if (plant) {
      whereSection = `${whereSection}plant ='${plant}'`;
    }

    const plantParametersQuery = `SELECT
    plp.id,
    plp.plant,
    plp.parameter,
    plp.is_active,
    plp.unit,
    parameter_type,
    type
    FROM plant_parameter plp
    INNER JOIN
    parameter ON parameter.name = plp.parameter
    ${whereSection == "where " ? "" : whereSection}`;

    const result = await pool.query(plantParametersQuery);

    // console.log(result.rows)
    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "current-user",
        data: error.message,
      })
    );
  }
};

export default plantParameters;
