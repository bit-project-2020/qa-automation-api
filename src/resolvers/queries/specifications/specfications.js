const specifications = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    const productsQuery = `SELECT * FROM specification WHERE product=${args.where.id}`;

    console.log(productsQuery);

    const result = await pool.query(productsQuery);
    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "specifications",
        data: error.message,
      })
    );
  }
};

export default specifications;
