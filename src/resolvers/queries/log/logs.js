const plants = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    let filteringBlock = "";
    // console.log(args.where);
    if (args.where && Object.keys(args.where).length !== 0) {
      filteringBlock = "WHERE ";
      const keys = Object.keys(args.where);
      keys.forEach((key) => {
        if (filteringBlock.length > 6) {
          filteringBlock = `${filteringBlock} AND `;
        }
        // handle greaterthan lower than thing
        if (key.includes("_gt")) {
          const keyPortion = key.split("_gt")[0];
          filteringBlock = `${filteringBlock}ul.${keyPortion} >= '${args.where[key]}'::timestamp`;
        } else if (key.includes("_lt")) {
          const keyPortion = key.split("_lt")[0];
          filteringBlock = `${filteringBlock}ul.${keyPortion} <= '${args.where[key]}'::timestamp`;
        } else {
          filteringBlock = `${filteringBlock}ul.${key}='${args.where[key]}'`;
        }
      });
    }

    const userlogQuery = `SELECT ul.*, u.first_name, u.last_name, u.profile_photo, u.role FROM user_log ul  JOIN "user" u
    ON ul.created_by = u.id ${filteringBlock}`;

    const result = await pool.query(userlogQuery);
    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "user_log",
        data: error.message,
      })
    );
  }
};

export default plants;
