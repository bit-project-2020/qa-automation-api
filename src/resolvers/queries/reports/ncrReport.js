import formatDate from "../../../lib/helpers.js/formatDate"

const ncr_report = async (parent, args, ctx, info) => {
  try {
    const { start_time, end_time } = args.where;
    const pool = ctx.postgresPool;

    const query = `SELECT date(created_at), plant, COUNT(id) as count  
    FROM ncr_data WHERE created_at BETWEEN '${start_time}' AND '${end_time}' GROUP BY plant, date(created_at);`;

    // looks like the query handles things ok
    const data = {}

    const result = await pool.query(query);

    // console.log(result.rows);

    result.rows.map((row) => {
      row.date = formatDate(row.date)
      if (Object.keys(data).includes(row.date)) {
        data[row.date].push({ plant: row.plant, count: row.count })
      }
      else {
        data[row.date] = [{ plant: row.plant, count: row.count }]
      }
    })

    data = Object.keys(data).map((key) => {
      return { "day": key, "data": data[key] }
    })

    return data;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "ncr_report",
        data: error.message,
      })
    );
  }
};

export default ncr_report;
