const report_deviation = async (parent, args, ctx, info) => {
  console.log(11111, args);
  const { plant, start_time, end_time } = args.where;
  try {
    const pool = ctx.postgresPool;

    const query = `SELECT p.parameter, COUNT(*)::Integer as "noOfIncidents"
     FROM
       "deviation"
     JOIN
      "plant_parameter" p
      ON
       deviation.plant_parameter = p.id
     where deviation.plant='${plant}' GROUP BY p.parameter`;

    // looks like the query handles things ok

    const result = await pool.query(query);
    console.log(result);
    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "report_deviation",
        data: error.message,
      })
    );
  }
};

export default report_deviation;
