const specificationDeviations = async (parent, args, ctx, info) => {
  try {
    const { product, plant, size, record_id } = args.where;
    // do the poolings here later
    const pool = ctx.postgresPool;

    const whereSeciton = ``;

    if (product != undefined) {
      whereSeciton =
        whereSeciton +
        `${whereSeciton.length > 0 ? "AND" : ""} deviation.product=${product} `; // dont forget to keep an string
    }
    if (plant != undefined) {
      whereSeciton =
        whereSeciton +
        `${whereSeciton.length > 0 ? "AND" : ""} deviation.plant='${plant}' `;
    }
    if (record_id != undefined) {
      whereSeciton =
        whereSeciton +
        `${
          whereSeciton.length > 0 ? "AND" : ""
        } deviation.record_id='${record_id}' `;
    }
    if (size != undefined) {
      whereSeciton =
        whereSeciton +
        `${whereSeciton.length > 0 ? "AND" : ""} deviation.size='${size}' `;
    }

    const specificationDeviationsQuery = `SELECT
      deviation.id,
      deviation.plant,
      deviation.product,
      deviation.value,
      deviation.specification,
      deviation.created_at,
      deviation.created_by,
      u.first_name as first_name,
      u.last_name as last_name,
      u.profile_photo as profile_photo,
      u.role,
      deviation.plant_parameter,
      plant_parameter.unit,
      deviation.record_id,
      deviation.size,
      product.name as product_name,
      parameter.name as parameter
    FROM deviation
      JOIN plant_parameter ON plant_parameter.id = deviation.plant_parameter
      JOIN parameter ON parameter.name = plant_parameter.parameter
      JOIN product ON deviation.product = product.id
      JOIN "user" u ON deviation.created_by = u.id
    WHERE ${whereSeciton} ORDER BY deviation.created_at DESC`;

    const result = await pool.query(specificationDeviationsQuery);

    console.log(22222222, result.rows);

    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "user_log",
        data: error.message,
      })
    );
  }
};

export default specificationDeviations;
