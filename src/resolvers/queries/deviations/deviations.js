const deviations = async (parent, args, ctx, info) => {
  try {
    const { product } = args.where;
    // do the poolings here later
    const pool = ctx.postgresPool;

    const deviationsQuery = `SELECT * FROM size_spec_deviations WHERE product='${product}' ORDER BY size DESC`;

    const result = await pool.query(deviationsQuery);

    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "user_log",
        data: error.message,
      })
    );
  }
};

export default deviations;
