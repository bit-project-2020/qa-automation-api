import path from 'path'
import fs from 'fs'

const backup = async (parent, args, ctx, info) => {
  try {
    const files = await fs.readdirSync('./programatic_dumps')

    const filesResponse = []
    if (files) {
      files.forEach(file => filesResponse.push({name: file}))

      return filesResponse
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'user_log', data: error.message })
    );
  }
};

export default backup;
