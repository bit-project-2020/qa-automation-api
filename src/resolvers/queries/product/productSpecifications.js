import populateSpecsForParams from "../../../lib/populateSpecsForParams";
const productSpecifications = async (parent, args, ctx, info) => {
  try {
    // known TODO
    // if the product hadn't requested size then it will send default specs to the frontend
    const pool = ctx.postgresPool;

    const { product_id, spec_type, product_size } = args.where;

    const productQuery = `SELECT
        id,
        plant,
        name,
        image_url,
        is_active FROM "product" where id='${product_id}'`;

    const sizeQuery = `SELECT
    id,
    size_name,
    is_active,
    product FROM "size" where id='${product_size}'`;

    const productOutput = await pool.query(productQuery);
    const sizesOutput = await pool.query(sizeQuery);

    const plant = productOutput.rows[0].plant;
    const size = sizesOutput.rows[0];

    const paramsWithSpecs = await populateSpecsForParams(
      { plant, product: product_id, size: product_size, spec_type },
      pool
    );

    return {
      product: productOutput.rows[0],
      size: size,
      plant_param_with_spec: paramsWithSpecs,
    };
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "product-specifications",
        data: error.message,
      })
    );
  }
};

export default productSpecifications;
