const products = async (parent, args, ctx, info) => {
  try {
    let filteringBlock = "";
    // console.log(args.where);
    if (args.where && Object.keys(args.where).length !== 0) {
      filteringBlock = "WHERE ";
      const keys = Object.keys(args.where);
      keys.forEach((key) => {
        if (filteringBlock.length > 6) {
          // make sure the AND is not inject to the first condition
          filteringBlock = `${filteringBlock} AND `;
        }
        filteringBlock = `${filteringBlock}${key}='${args.where[key]}'`;
      });
    }

    const pool = ctx.postgresPool;

    const productsQuery = `SELECT * FROM "product" ${filteringBlock}`;
    console.log(productsQuery);

    const result = await pool.query(productsQuery);
    return result.rows;
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "products",
        data: error.message,
      })
    );
  }
};

export default products;
