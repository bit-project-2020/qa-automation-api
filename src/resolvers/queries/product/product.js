const product = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;


    const productQuery = `SELECT * FROM "product" where id='${args.where.id}'`;
    // console.log(args);

    const result = await pool.query(productQuery);
    // console.log(result);
    return result.rows[0];

  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'product', data: error.message })
    );
  }
};

export default product;
