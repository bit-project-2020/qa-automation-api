const updateDeviation = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { is_active } = args.data;
      const { id } = args.where;

      // build the query param set
      const is_active_formatted = typeof is_active === 'boolean' ?`, is_active = '${is_active}'` : '';

      const full_param_set = `${is_active_formatted}`;
      // remove the first comma of the param set
      full_param_set = full_param_set.replace(',', '');
      const queryText = `UPDATE size_spec_deviations SET ${full_param_set} where id =${id} RETURNING *`;

       console.log(queryText);

      await pool.query("BEGIN");
      const result = await pool.query(queryText);
      await pool.query("COMMIT");
      console.log(result.rows[0]);

      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "update-deviations",
        data: error.message,
      })
    );
  }
};

export default updateDeviation;
