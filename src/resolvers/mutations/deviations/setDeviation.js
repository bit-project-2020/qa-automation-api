const setDeviation = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { product, plant_parameter, size, is_active } = args.data;
      console.log(args.data);

      await pool.query('BEGIN');
      // insert into plants database
      const queryText = `INSERT INTO size_spec_deviations(product, plant_parameter, size, is_active) VALUES('${product}', '${plant_parameter}', '${size}', '${is_active}' ) RETURNING *`
      
      const result = await pool.query(queryText);

      console.log(result);
      await pool.query('COMMIT');
      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query('ROLLBACK');
      throw e
    } 
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'create-deviation', data: error.message })
    );
  }
};

export default setDeviation;
