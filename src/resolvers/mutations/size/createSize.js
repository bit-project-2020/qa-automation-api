import { query } from "express";

const createSize = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { size_name, product, is_active } = args.data;
      await pool.query("BEGIN");

      const queryText =
        `INSERT INTO size(` +
        `size_name,` +
        `is_active,` +
        `product,` +
        `) VALUES(` +
        `'${size_name}',` +
        `'${is_active}',` +
        `'${product}',)` +
        `RETURNING *`;

      // remove the last comma before the closing bracket
      queryText = queryText.replace(/\,\)/g, ")");


      const result = await pool.query(queryText);

      await pool.query("COMMIT");
      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-size",
        data: error.message,
      })
    );
  }
};

export default createSize;
