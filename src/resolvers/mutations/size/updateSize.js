const updateSize = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { size_name, is_active } = args.data;

      const is_active_formatted = typeof is_active === 'boolean' ?`, is_active = '${is_active}'` : ''
      
      const { id } = args.where;
      await pool.query("BEGIN");

      const queryText = `UPDATE size SET size_name = '${size_name}' ${is_active_formatted} where id ='${id}'  RETURNING *`;

      const result = await pool.query(queryText);

      await pool.query("COMMIT");
      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "update-plant-parameter",
        data: error.message,
      })
    );
  }
};

export default updateSize;
