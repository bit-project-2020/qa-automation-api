import { or } from "graphql-shield";

const createPlantParameter = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { plant, parameter, is_active, unit } = args.data;

      await pool.query("BEGIN");

      // TODO get the type from the parameters table by passing the parameter.
      const parameterTypeQuery = `SELECT parameter_type,type FROM parameter WHERE name='${parameter}'`;
      const paramTypeResult = await pool.query(parameterTypeQuery);
      const { parameter_type, type } =
        paramTypeResult.rows && paramTypeResult.rows[0];

      console.log(2222, parameter_type);

      if (!parameter_type || !type) {
        return new Error(
          JSON.stringify({
            type: "db-error",
            source: "create-plant-parameter",
            data: "failed-finding-parameters-type",
          })
        );
      }

      const queryText = `INSERT INTO plant_parameter(plant, parameter, unit, is_active) VALUES('${plant}','${parameter}','${unit}','${is_active}') RETURNING *`;
      console.log(queryText);
      const result = await pool.query(queryText);

      const types = {
        TEXT: "varchar",
        NUMBER: "float",
        NUMBER_EXACT: "float",
        DATE: "timestamp",
        BOOLEAN: "boolean",
        IMAGE: "varchar",
      };

      // make sure database is ready to store that param
      const updateDatabaseQuery = `ALTER table ${plant}${
        type === "PACKAGING" ? "_packaging" : "_process"
      } ADD COLUMN "${parameter}" ${types[parameter_type]}`;
      console.log(3333, updateDatabaseQuery);
      await pool.query(updateDatabaseQuery);

      await pool.query("COMMIT");
      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-plant-parameter",
        data: error.message,
      })
    );
  }
};

export default createPlantParameter;
