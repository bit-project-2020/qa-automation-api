const updatePlantParameter = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { is_active } = args.data;
      const { id } = args.where;
      await pool.query('BEGIN');

      const queryText = `UPDATE plant_parameter SET is_active=${is_active} where id ='${id}'  RETURNING *`

      const result = await pool.query(queryText);

      
      await pool.query('COMMIT');
      console.log(queryText)
      return result.rows && result.rows[0];
    } catch (e) {
      console.log(e)
      await pool.query('ROLLBACK');
      throw e
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'update-plant-parameter', data: error.message })
    );
  }
};

export default updatePlantParameter;
