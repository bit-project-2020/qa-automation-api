const deletePlantParameter = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;
    const { id } = args.where;

    await pool.query("BEGIN");

    const fetchPlantParameter = `SELECT * from plant_parameter WHERE id='${id}'`;
    const fetchParamResult = await pool.query(fetchPlantParameter);

    const queryText = `DELETE FROM plant_parameter where id='${id}' RETURNING *`;
    const result = await pool.query(queryText);

    if (!fetchParamResult.rows[0]) {
      throw new Error("no plant parameter found");
    }

    const parameter = fetchParamResult.rows[0];

    // query that spec parameter type
    const parameterTypeQuery = `SELECT parameter_type,type FROM parameter WHERE name='${parameter.parameter}'`;
    const paramTypeResult = await pool.query(parameterTypeQuery);
    const { parameter_type, type } =
      paramTypeResult.rows && paramTypeResult.rows[0];

    if (!parameter_type || !type) {
      return new Error(
        JSON.stringify({
          type: "db-error",
          source: "create-plant-parameter",
          data: "failed-finding-parameters-type",
        })
      );
    }

    const plant = fetchParamResult.rows && fetchParamResult.rows[0].plant;

    // get plant_parameter from spec parameters query
    const queryText = `DELETE FROM plant_parameter where id='${id}' RETURNING *`;
    const result = await pool.query(queryText);

    const parameter =
      fetchParamResult.rows && fetchParamResult.rows[0].parameter;

    // make sure database is ready to store that param
    const updateDatabaseQuery = `ALTER table ${plant}${
      type === "PACKAGING" ? "_packaging" : "_process"
    } DROP COLUMN "${parameter}"`;

    await pool.query(updateDatabaseQuery);

    await pool.query("COMMIT");
    return result.rows && result.rows[0];
  } catch (error) {
    await pool.query("ROLLBACK");
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "delete-plant-parameter",
        data: error.message,
      })
    );
  }
};

export default deletePlantParameter;
