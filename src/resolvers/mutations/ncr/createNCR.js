const createNCR = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    console.log("I ran");
    try {
      const { plant, product, size, type, record_id, is_active } = args.data;
      await pool.query("BEGIN");

      // console.log(30001000, ctx.user.id);

      const is_active_formatted =
        is_active !== undefined ? `,'${is_active}'` : ``;

      // return;
      const queryText =
        `INSERT INTO ncr_data(` +
        `plant,` +
        `product,` +
        `size,` +
        `type,` +
        `created_by,` +
        `record_id,` +
        // `is_active` +
        `${is_active != undefined ? ",is_active" : ""}` +
        `) VALUES(` +
        `'${plant}',` +
        `'${product}',` +
        `'${size}',` +
        `'${type}',` +
        `'${ctx.user.id}',` +
        `'${record_id}'` +
        `${is_active_formatted})` +
        `RETURNING *`;

      // remove the last comma before the closing bracket
      queryText = queryText.replace(/\,\)/g, ")");

      console.log(444444, queryText);

      const result = await pool.query(queryText);

      await pool.query("COMMIT");
      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-ncr",
        data: error.message,
      })
    );
  }
};

export default createNCR;
