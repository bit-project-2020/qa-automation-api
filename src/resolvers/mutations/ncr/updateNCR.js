const updateNCR = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const {
        qa_representative,
        deviated_parameters,
        reason,
        production_representative,
        is_active,
        comment,
      } = args.data;
      const { id } = args.where;

      // build the query param set
      const qa_representative_formatted =
        qa_representative === null
          ? ` qa_representative = NULL`
          : typeof qa_representative === "number"
          ? `, qa_representative = ${qa_representative}`
          : "";
      const deviated_parameters_formatted =
        typeof deviated_parameters === "string"
          ? `, deviated_parameters = '${deviated_parameters}'`
          : "";
      const reason_formatted =
        typeof reason === "string" ? `, reason = '${reason}'` : "";
      const production_representative_formatted =
        production_representative === null
          ? ` production_representative = NULL`
          : typeof production_representative === "number"
          ? `, production_representative = ${production_representative}`
          : "";
      const is_active_formatted =
        typeof is_active === "boolean" ? `, is_active = '${is_active}'` : "";
      const comment_formatted =
        typeof comment === "string" ? `, comment = '${comment}'` : "";

      const full_param_set = `${qa_representative_formatted}${deviated_parameters_formatted}${reason_formatted}${production_representative_formatted}${is_active_formatted}${comment_formatted}`;
      // remove the first comma of the param set
      full_param_set = full_param_set.replace(",", "");
      const queryText = `UPDATE ncr_data SET ${full_param_set} where id= ${id} RETURNING *`;

      // console.log(queryText);

      await pool.query("BEGIN");
      const result = await pool.query(queryText);
      await pool.query("COMMIT");
      // console.log(result.rows[0]);

      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "update-ncr",
        data: error.message,
      })
    );
  }
};

export default updateNCR;
