const creaetLog = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool
    const created_by = ctx.user.id

    // get the current time and select the shift.

    const selectedShift = "shift-1"

    const currentTimestamp = new Date()
    const currentHour = currentTimestamp.getHours()

    if (currentHour > 6 && currentHour < 14) {
      selectedShift = "shift-1"
    } else if (currentHour > 14 && currentHour < 22) {
      selectedShift = "shift-2"
    } else {
      selectedShift = "shift-3"
    }

    try {
      const { plant, title, log } = args.data
      await pool.query("BEGIN")
      // insert into plants database
      const queryText = `INSERT INTO user_log(plant, title, log, created_by, shift) VALUES('${plant}', '${title}', '${log}', '${created_by}', '${selectedShift}' ) RETURNING *`

      const result = await pool.query(queryText)

      await pool.query("COMMIT")
      return result.rows && result.rows[0]
    } catch (e) {
      await pool.query("ROLLBACK")
      throw e
    }
  } catch (error) {
    console.log(error)
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-plant",
        data: error.message,
      })
    )
  }
}

export default creaetLog
