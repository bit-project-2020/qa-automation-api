import sendMail from "../../../lib/sendMail";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

const resetPassword = async (parent, args, ctx, info) => {
  let user;

  try {
    const pool = ctx.postgresPool;
    await pool.query("BEGIN");

    const requestedUser = `SELECT * FROM "user" WHERE otp='${args.otp}' AND otp_expiry >= NOW()`;
    const result = await pool.query(requestedUser);

    if (result && result.rowCount >= 1) {
      user = result.rows[0];
    } else {
      throw new Error("OTP expired or not found");
    }

    // encrypt the given password and store it there
    const password = await bcrypt.hash(args.newPassword, 10);

    const postUserUpdateQuery = `UPDATE "user" SET password='${password}', otp=NULL, otp_expiry=NULL WHERE id=${user.id}`;

    const result = await pool.query(postUserUpdateQuery);

    // if needed log the usre in
    const token = jwt.sign({ id: user.id }, process.env.APP_SECRET);

    ctx.response.cookie(
      "token",
      token,
      {
        // store a cookie in the client browser
        httpOnly: false,
        maxAge: 1000 * 60 * 60 * 24 * 30, // one month cookie
      },
      "/"
    );

    // send the password confirmation login
    const messageHtml = `
    <p>Dear ${user.first_name},</p>
    <p><span style="color: #ff0000;"> Your password updated succesfully, please use new credentials </span>
    if you didn't change the password, contact the administrator immediately</p>
    <p>Thank You,<br />Chandima sweets management</p>`;

    await sendMail({
      emailFrom: '"management 🤵‍ " <management@chandimasweets.online>',
      emailTo: user.email,
      htmlPayload: messageHtml,
      subject: "QA automation System password reset",
      textPayload: "please use a html supported browser",
    });

    // send success message to the frontend

    await pool.query("COMMIT");
    return {
      status: "success",
      message: "password updated succesfully",
    };
  } catch (error) {
    await pool.query("ROLLBACK");
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "password-reset",
        data: error.message,
      })
    );
  }
};

export default resetPassword;
