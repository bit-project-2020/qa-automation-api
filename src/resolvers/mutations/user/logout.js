const logout = async (parent, args, ctx, info) => {
  try {
    const token = null;

    const appname = ctx.request.headers.appname;

    const cookie = ctx.response.cookie(
      appname == "Admin"
        ? "admin_token"
        : appname == "User"
        ? "user_token"
        : "token",
      token,
      {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24, // 1 day cookie
      }
    );

    return {
      status: "Success",
      message: "Successfully logged out the user",
    };
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-plant",
        data: error.message,
      })
    );
  }
};

export default logout;
