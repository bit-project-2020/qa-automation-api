import sendMail from "../../../lib/sendMail";
import randomPass from "../../../lib/randomPassword";

const requestPasswordReset = async (parent, args, ctx, info) => {
  console.log("reset password called");
  console.log(args);

  try {
    let user;

    const pool = ctx.postgresPool;
    // generate a random token
    const randToken = randomPass(4);

    const setPasswordResetToken = `UPDATE "user"
    SET
      otp = '${randToken}',
      otp_expiry = now() + interval '1 day'
    WHERE
      email ='${args.email}' RETURNING *`;

    const result = await pool.query(setPasswordResetToken);

    if (result && result.rowCount >= 1) {
      user = result.rows[0].first_name;
    } else {
      throw new Error("Invalid email");
    }

    // then if email exists send the password reset token through that email
    const messageHtml = `
    <p>Hi ${user},</p>
    <p>Please Use <span style="color: #ff0000;">${randToken}</span> as your password reset OTP. this code expires in 1 day</p>
    <p>Thank You,<br />Chandima sweets management</p>`;

    await sendMail({
      emailFrom: '"management 🤵‍ " <management@chandimasweets.online>',
      emailTo: args.email,
      htmlPayload: messageHtml,
      subject: "QA automation System password reset token",
      textPayload: "please use a html supported browser",
    });

    // send success message to the frontend
    return {
      status: "success",
      message: "sent otp to the email",
    };
  } catch (error) {
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "request-password-reset",
        data: error.message,
      })
    );
  }
};

export default requestPasswordReset;
