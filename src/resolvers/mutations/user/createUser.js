import bcrypt from "bcryptjs";

import { RandomPass } from "../../../lib/randomPassword";
import sendMail from "../../../lib/sendMail";

const createUser = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;
    // get all necesary data from the call
    const {
      first_name,
      last_name,
      role,
      email,
      profile_photo,
      is_active,
    } = args.data;

    // generate 5 letter random password
    const randomPass = RandomPass(5);

    console.log(randomPass);

    const profile_photo_formatted =
      profile_photo !== undefined ? `,'${profile_photo}'` : ``;
    const is_active_formatted =
      is_active !== undefined ? `,'${is_active}'` : ``;

    // hashing the password
    const password = await bcrypt.hash(randomPass, 10);

    // insert into plants database
    const createUserQuery =
      `INSERT INTO "user" 
    (first_name, last_name, role, password, email` +
      `
     ${profile_photo != undefined ? ",profile_photo" : ""}` +
      `
      ${is_active != undefined ? ",is_active" : ""}` +
      `
    )
    VALUES('${first_name}', '${last_name}', '${role}', '${password}', '${email}' ${profile_photo_formatted} ${is_active_formatted}) 
    RETURNING *`;

    console.log(400100, createUserQuery);

    const result = await pool.query(createUserQuery);

    // if user creation succesful
    const messageHtml = `
        <h2>Dear, ${
          first_name.charAt(0).toUpperCase() +
          first_name.slice(1) /* this will capitalize the first letter*/
        }</h2> 
        <h3>Welcome to Chandima Sweets QA portal <span class="fr-emoticon fr-deletable fr-emoticon-img" style="background: url(https://cdnjs.cloudflare.com/ajax/libs/emojione/2.0.1/assets/svg/1f600.svg);">&nbsp;</span></h3>
        <p>Your account has been created.<br>
        use  <strong>${randomPass}</strong> as a tempory password to login</p><p>you may change your password</p>
      `;

    // send the email now
    await sendMail({
      emailFrom: '"management 🤵‍ " <management@chandimasweets.online>',
      emailTo: email,
      htmlPayload: messageHtml,
      subject: "Welcome to ChandimaSweets QA automation System",
      textPayload: "please use a html supported browser",
    });

    return result.rows && result.rows[0];
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-user",
        data: error.message,
      })
    );
  }
};

export default createUser;
