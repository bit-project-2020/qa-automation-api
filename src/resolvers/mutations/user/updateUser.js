const updateUser = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const {
        first_name,
        last_name,
        role,
        department,
        emp_no,
        user_name,
        email,
        profile_photo,
        is_active,
      } = args.data;
      const { id } = args.where;

      // build the query param set
      const firstname_formatted =
        typeof first_name === "string" ? `, first_name = '${first_name}'` : "";

      const is_active_formatted =
        typeof is_active === "boolean" ? `, is_active = ${is_active}` : "";

      const lastnme_formatted =
        typeof last_name === "string" ? `, last_name = '${last_name}'` : "";
      const role_formatted =
        typeof role === "string" ? `, role = '${role}'` : "";
      const emp_no_formatted =
        typeof emp_no === "string" ? `, emp_no = '${emp_no}'` : "";
      const user_name_formatted =
        typeof user_name !== "undefined"
          ? `, user_name = ${user_name === null ? "NULL" : `'${user_name}'`}`
          : "";
      const profile_photo_formatted =
        typeof profile_photo !== "undefined"
          ? `, profile_photo = ${
              profile_photo === null ? "NULL" : `'${profile_photo}'`
            }`
          : "";

      let department_formatted = "";
      if (typeof department === "string") {
        department_formatted = `, department = '${department}' `;
      } else if (department === null) {
        department_formatted = `, department = 'NULL' `;
      }

      let email_formatted = "";
      if (typeof email === "string") {
        email_formatted = `, email = '${email}' `;
      } else if (email === null) {
        email_formatted = `, email = 'NULL' `;
      }

      const full_param_set = `${firstname_formatted}${is_active_formatted}${lastnme_formatted}${role_formatted}${emp_no_formatted}${user_name_formatted}${department_formatted}${email_formatted}${profile_photo_formatted}`;
      // remove the first comma of the param set
      full_param_set = full_param_set.replace(",", "");

      // here I learned something I had to use quotes around user because it is a keyword in postgres. so to avoid conflicting with the user table had to quote it.
      const queryText = `UPDATE "user" SET${full_param_set} WHERE id = ${id} RETURNING *`;

      console.log(queryText);

      await pool.query("BEGIN");
      const result = await pool.query(queryText);
      await pool.query("COMMIT");
      // console.log(result.rows[0]);

      return result.rows && result.rows[0];
    } catch (e) {
      console.log(e);
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "update-user",
        data: error.message,
      })
    );
  }
};

export default updateUser;
