// returning errors instead throwing errors. because we need to show the frontend those exact error messages
// if we throw it will catch by the catch block and modify the message

import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

const login = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool; // connect to the postgres pool
    const { email, password } = args; // get the email and password from the request.

    // get the user and password from the database
    const userQuery = `SELECT * FROM "user" WHERE email = '${email}'`;

    const result = await pool.query(userQuery);

    const user = result.rows[0];

    if (!user) {
      // If there is no user, send no-user-found to the frontend application.
      return new Error("Invalid Credentials");
    }

    // use bcrypt module to compare the user typed password with the hashed password
    const match = await bcrypt.compare(password, user.password);

    // if the password doesn't match return an error
    if (!match) {
      return new Error("Invalid Credentials");
    }

    // if the user account is deactivated then tell your account has been deactivated contact system admin
    if (user.is_active === false) {
      return new Error("Your Account has been deactivated");
    }

    const appname = ctx.request.headers.appname;
    // if a wrong user trying to login to a wrong app then tell that user to the login to the correct app
    if (appname === "Admin") {
      if (user.role === "QA_OFFICER") {
        return new Error(
          "You are not authorized to login this app. Please use the QA app instead!"
        );
      }
    }

    if (appname === "User") {
      if (
        [
          "QA_MANAGER",
          "PRODUCTION_OFFICER",
          "PRODUCTION_MANAGER",
          "QA_EXECUTIVE",
          "SYSTEM_ADMIN",
        ].includes(user.role)
      ) {
        return new Error(
          "You are not authorized to login this app. Please use the User Admin instead!"
        );
      }
    }

    // sign a jwt token and set the cookie
    // app secret is loaded from the .env file using dotenv module.
    const token = jwt.sign({ id: user.id }, process.env.APP_SECRET);

    ctx.response.cookie(
      appname == "Admin"
        ? "admin_token"
        : appname == "User"
        ? "user_token"
        : "token",
      token,
      {
        // store a cookie in the client browser
        httpOnly: false,
        sameSite: null,
        maxAge: 1000 * 60 * 60 * 24 * 30, // one month cookie
      },
      "/"
    );

    return result.rows && result.rows[0];
  } catch (error) {
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "login",
        data: error.message,
      })
    );
  }
};

export default login;
