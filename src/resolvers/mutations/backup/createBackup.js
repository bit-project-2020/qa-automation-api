import child_process from "child_process"
import util from "util"
const exec = util.promisify(child_process.exec)

const createBackup = async (parent, args, ctx, info) => {
  try {
    const postgresUser = process.env.POSTGRES_USER;
    const postgresPassword = process.env.POSTGRES_PASSWORD;
    const postgresContainer = process.env.POSTGRES_CONTAINER_NAME;

    const currentDate = `${new Date().toISOString()}`
    // console.log(1111,currentDate);

    const { stdout, stderr } = await exec(
      // `docker exec -t -u ${postgresUser} ${postgresContainer} pg_dumpall  -c > ./programatic_dumps/${currentDate}_backup.sql`,
      //`docker exec -t -u ${postgresUser} ${postgresContainer} pg_dumpall > ./programatic_dumps/${currentDate}_backup.sql`, // stopped cleaning. 
      // `docker exec -t -u ${postgresUser} ${postgresContainer} pg_dump --schema=public --format=c -f /backups/backup_${currentDate}.tar -F t`,
      // `docker exec -t -u ${postgresUser} ${postgresContainer} sudo pg_dump --clean --format=c --encoding=UTF-8 -f /backups/backup.sql -n public postgres`,
      // `docker exec -e PGPASSWORD=${postgresPassword} -t -u ${postgresUser} ${postgresContainer} pg_dump --clean --format=c --encoding=UTF-8 -f /backups/backup.sql -n public postgres`,
      // `export PGPASSWORD=${postgresPassword} && pg_dump -Fc -h localhost -p 5415 -U ${postgresUser} --clean --format=c -f ./programatic_dumps/${currentDate}_backup.sql postgres`,
      `export PGPASSWORD=${postgresPassword} && pg_dump -Fc -h localhost -p 5415 -U ${postgresUser} --format=c -f ./programatic_dumps/${currentDate}_backup.sql postgres`, // have removed cleaning from above command
    );

   
      if (stderr) {
        console.log(stderr);
        return {
          message: `backup failed!`,
          status: "Failure!"
        }
      } else {
        console.log(stdout);
        console.log("Backup complete");
      }
    

    return {
      message: `backup file ${currentDate}_backup created succesfuly!`,
      status: "Success!"
    };
   
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "backup",
        data: error.message,
      })
    );
  }
};

export default createBackup;
