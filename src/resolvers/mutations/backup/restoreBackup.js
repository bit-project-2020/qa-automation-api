import child_process from "child_process"
import util from "util"

const exec = util.promisify(child_process.exec)

const restoreBackup = async (parent, args, ctx, info) => {
  try {
    const postgresUser = process.env.POSTGRES_USER
    const postgresPassword = process.env.POSTGRES_PASSWORD;
    const postgresContainer = process.env.POSTGRES_CONTAINER_NAME


    // this function does show stuff as stderr even some operations failed.

    try {
      const { stdout, stderr } = await exec(
        //`cat ./programatic_dumps/backup.sql | docker exec -i ${postgresContainer} psql -U ${postgresUser}`,
  
        // `docker exec -t -u ${postgresUser} ${postgresContainer} psql -U postgres -h localhost -d postgres -c "DROP SCHEMA IF EXISTS public cascade" && pg_restore --verbose --username=${postgresUser} --format=c --dbname=postgres /backups/backup.sql`,
        // `docker exec -t -u ${postgresUser} ${postgresContainer} pg_restore --verbose --username=${postgresUser} --format=c --dbname=postgres /backups/backup.sql`,
         // `docker exec -t -u ${postgresUser} ${postgresContainer} psql -U postgres -h localhost -d postgres -c "DROP SCHEMA IF EXISTS public cascade" && ls && docker exec -t -u ${postgresUser} ${postgresContainer} pg_restore --verbose --format=c --dbname=postgres -h localhost /programatic_dumps/${args.backupID}`,
        // `docker exec -t -u ${postgresUser} ${postgresContainer} psql -U postgres -h localhost -d postgres -c "DROP SCHEMA IF EXISTS public cascade" && cat ./programatic_dumps/${args.backupID} | docker exec -i ${postgresContainer} psql -U ${postgresUser}`,
        // `docker exec -t -u ${postgresUser} ${postgresContainer} psql -U postgres -h localhost -d postgres -c "DROP SCHEMA IF EXISTS public cascade" && docker exec -t -u ${postgresUser} ${postgresContainer} pg_restore --verbose --format=c --dbname=postgres -h localhost /backups/backup2.sql`, //${args.backupID}
        //`docker exec -t -u ${postgresUser} ${postgresContainer} psql -U postgres -h localhost -d postgres -c "DROP SCHEMA IF EXISTS public cascade" && docker exec -t -u ${postgresUser} ${postgresContainer} pg_restore --verbose --format=c --dbname=postgres -h localhost /backups/${args.backupID}`, //${args.backupID}
        //`docker exec -t -u ${postgresUser} ${postgresContainer} psql -U postgres -h localhost -d postgres -c "DROP SCHEMA IF EXISTS public cascade" && docker exec -t -u ${postgresUser} ${postgresContainer} pg_restore --verbose --dbname=postgres -h localhost /backups/${args.backupID}`, // lets omit --format coz it is already done when creating
        // `docker exec ${postgresContainer} psql -U ${postgresUser} -f /backups/${args.backupID}`,
        // `docker exec -t -u ${postgresUser} ${postgresContainer} pg_restore --verbose --dbname=postgres -h --clean --format=c localhost /backups/${args.backupID}`, // lets try without deleting the schema
        `export PGPASSWORD=${postgresPassword} && pg_restore -h localhost -p 5415 -U ${postgresUser} --verbose --dbname=postgres --clean --format=c ./programatic_dumps/${args.backupID}`, // lets connect directly
      );
  
      if (!stderr) {
        console.log(999, "looks like no error")
        console.log(stdout)
      } else {
        console.log(stderr)
        // return {
        //   message: "restore failed",
        //   status: "failure"
        // }
      }
    } catch (e) {
      // console.log(e);
    }

    return {
      message: "restore succesful",
      status: "success"
    }
  } catch (error) {
    console.log(error)
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "backup",
        data: error.message,
      })
    )
  }
}

export default restoreBackup
