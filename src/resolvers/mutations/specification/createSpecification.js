import { query } from "express"

const createSpecification = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool

    try {
      const {
        product,
        plant_parameter,
        value,
        is_default,
        is_active,
        size,
      } = args.data

      await pool.query("BEGIN")

      const size_formatted =
      size !== undefined ? `'${size}',` : ``

      const queryText =
        `INSERT INTO specification(` +
        `product ,` +
        `plant_parameter ,` +
        `value ,` +
        `is_default, ` +
        `${size != undefined ? "size, " : ""}` +
        `is_active` +
        `) VALUES(` +
        `'${product}', ` +
        `'${plant_parameter}', ` +
        `'${value}', ` +
        `${is_default}, ` +
        `${size_formatted} ` +
        `${is_active})` +
        `RETURNING *`

      
      console.log(700620, queryText)

      // remove the last comma before the closing bracket
      queryText = queryText.replace(/\,\)/g, ")")

      const result = await pool.query(queryText)

      await pool.query("COMMIT")
      return result.rows && result.rows[0]
    } catch (e) {
      await pool.query("ROLLBACK")
      throw e
    }
  } catch (error) {
    console.log(error)
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-specification",
        data: error.message,
      })
    )
  }
}

export default createSpecification
