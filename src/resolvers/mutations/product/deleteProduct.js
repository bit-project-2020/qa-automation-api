const deleteProduct = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { id } = args.where;

      await pool.query('BEGIN');

      const result = await pool.query(`DELETE FROM product where id='${id}' RETURNING *`);

      await pool.query('COMMIT');
      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query('ROLLBACK');
      throw e
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'delete-product', data: error.message })
    );
  }
};

export default deleteProduct;
