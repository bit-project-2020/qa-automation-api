const updateProduct = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { plant, name, is_active, image_url } = args.data;
      const { id } = args.where;

      // build the query param set
      const image_url_formatted =
        typeof image_url === "string"
          ? `, image_url = '${image_url}'`
          : `, image_url = NULL`;
      const is_active_formatted =
        typeof is_active === "boolean" ? `, is_active = '${is_active}'` : "";
      const plant_formatted =
        typeof plant === "string" ? `, plant = '${plant}'` : "";

      const name_formatted =
        typeof name === "string" ? `, name = '${name}'` : "";

      const full_param_set = `${name_formatted}${is_active_formatted}${image_url_formatted}${plant_formatted}`;
      // remove the first comma of the param set
      full_param_set = full_param_set.replace(",", "");
      const queryText = `UPDATE product SET ${full_param_set} where id =${id} RETURNING *`;

      console.log(queryText);

      await pool.query("BEGIN");
      const result = await pool.query(queryText);
      await pool.query("COMMIT");

      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "update-product",
        data: error.message,
      })
    );
  }
};

export default updateProduct;
