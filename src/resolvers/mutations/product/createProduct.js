import { query } from "express";

const createProduct = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { plant, name, is_active, image_url } = args.data;
      await pool.query("BEGIN");

      const is_active_formatted =
        is_active !== undefined ? `,'${is_active}'` : ``;
      const image_url_formatted =
        image_url !== undefined ? `,'${image_url}'` : ``;

      const queryText =
        `INSERT INTO product(` +
        `plant,` +
        `name` +
        `${image_url != undefined ? ",image_url" : ""}` +
        `${is_active != undefined ? ",is_active" : ""}` +
        `) VALUES(` +
        `'${plant}',` +
        `'${name}'` +
        `${image_url_formatted}` +
        `${is_active_formatted})` +
        `RETURNING *`;

      // remove the last comma before the closing bracket
      queryText = queryText.replace(/\,\)/g, ")");

      console.log(queryText);

      const result = await pool.query(queryText);

      await pool.query("COMMIT");
      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-product",
        data: error.message,
      })
    );
  }
};

export default createProduct;
