const createPlant = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    try {
      const { plant_name, is_active, image_url } = args.data;

      console.log(400210, args.data);

      // if image url is null don't set it.
      const image_url_formatted =
        image_url !== undefined ? `'${image_url}'` : ``;

      await pool.query("BEGIN");
      // insert into plants database
      const queryText =
        `INSERT INTO plant(plant_name, is_active ` +
        `${image_url != undefined ? ",image_url" : ""}` +
        `) VALUES('${plant_name}', '${is_active}',` +
        `${image_url_formatted}` +
        `) RETURNING *`;

      // remove the last comma before the closing bracket
      queryText = queryText.replace(/\,\)/g, ")");

      console.log(550000, queryText);

      // create packaging data table
      const createPackagingDataTable =
        `CREATE TABLE ` +
        plant_name +
        `_packaging(
        id serial PRIMARY KEY,
        user_id integer REFERENCES "user"(id)  NOT NULL,
        approved_by integer REFERENCES "user"(id),
        size integer REFERENCES size(id) NOT NULL,
        product integer REFERENCES product(id) NOT NULL,
        shift varchar REFERENCES shift(name) NOT NULL,
        is_deviated boolean DEFAULT false,
        is_cancelled boolean DEFAULT false,
        comment varchar,
        type varchar DEFAULT 'PACKAGING',
        date timestamptz NOT NULL DEFAULT NOW()
      )`;

      const createProductDataTable =
        `CREATE TABLE ` +
        plant_name +
        `_process (
        id serial PRIMARY KEY,
        user_id integer REFERENCES "user"(id) NOT NULL,
        approved_by integer REFERENCES "user"(id),
        size integer REFERENCES size(id) NOT NULL,
        product integer REFERENCES product(id) NOT NULL,
        shift varchar REFERENCES shift(name) NOT NULL,
        is_deviated boolean DEFAULT false,
        is_cancelled boolean DEFAULT false,
        comment varchar,
        type varchar DEFAULT 'PROCESS',
        date timestamptz NOT NULL DEFAULT NOW()
      )`;

      // // create product and packaging data tables
      await pool.query(createPackagingDataTable);
      await pool.query(createProductDataTable);
      const result = await pool.query(queryText);

      await pool.query("COMMIT");
      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-plant",
        data: error.message,
      })
    );
  }
};

export default createPlant;
