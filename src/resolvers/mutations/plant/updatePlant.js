const updatePlant = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    console.log(args);

    try {
      const { image_url, is_active } = args.data;
      const { plant_name } = args.where;

      // build the query param set
      const image_url_formatted =
        typeof image_url === "string"
          ? `, image_url = '${image_url}'`
          : `, image_url = NULL`;
      const is_active_formatted =
        typeof is_active === "boolean" ? `, is_active = '${is_active}'` : "";

      const full_param_set = `${image_url_formatted}${is_active_formatted}`;

      console.log(full_param_set);
      // remove the first comma of the param set
      full_param_set = full_param_set.replace(",", "");
      const queryText = `UPDATE plant SET${full_param_set} where plant_name= '${plant_name}' RETURNING *`;

      console.log(queryText);

      await pool.query("BEGIN");
      const result = await pool.query(queryText);
      await pool.query("COMMIT");

      return result.rows && result.rows[0];
    } catch (e) {
      await pool.query("ROLLBACK");
      throw e;
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "update-ncr",
        data: error.message,
      })
    );
  }
};

export default updatePlant;
