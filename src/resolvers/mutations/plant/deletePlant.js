const DeletePlant = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;
    const { plant_name } = args;
    try {
      await pool.query('BEGIN');

      const deleteProductTable = `DROP TABLE ` + plant_name + `_packaging`;
      const deleteProcessTable = `DROP TABLE ` + plant_name + `_process`;

      // delete product and packaging tables
      await pool.query(deleteProductTable);
      await pool.query(deleteProcessTable);

      const result = await pool.query(`DELETE FROM plant where plant_name='${plant_name}' RETURNING *`);

      await pool.query('COMMIT');
      return result.rows && result.rows[0];
    } catch( e) {
      await pool.query('ROLLBACK');
      throw e
    }
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'current-user', data: error.message })
    );
  }
};

export default DeletePlant;
