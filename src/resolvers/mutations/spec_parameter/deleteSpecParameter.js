import plants from "../../queries/plant/plants";

const deleteSpecParameter = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;
    
    const { name } = args.where;

    await pool.query('BEGIN');

    // if the parameter is used by any plant then prevent it from deleting.
    const checkPlantParamExistance = `SELECT * FROM plant_parameter WHERE parameter='${name}'`;

    const existance = await pool.query(checkPlantParamExistance);
    if (existance.rowCount > 0) {
      plants = [];
      existance.rows.map(row => plants.push(row.plant));

      return new Error(`following plants are using this parameter. ${plants.join()}`);
    }

    const deleteSpecParameterQuery = `DELETE FROM "parameter" 
    WHERE name = '${name}'
    RETURNING *`;

    const result = await pool.query(deleteSpecParameterQuery);

    await pool.query('COMMIT');

    return result.rows && result.rows[0];
  } catch (error) {
    await pool.query('ROLLBACK');
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'delete-spec-param', data: error.message })
    );
  }
};

export default deleteSpecParameter;