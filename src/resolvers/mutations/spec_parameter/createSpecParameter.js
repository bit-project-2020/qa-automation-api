const createSpecParameter = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;
    
    const { name, parameter_type, type } = args.data;

    // insert into plants database
    const createParameterQuery = `INSERT INTO "parameter" 
    (name, parameter_type, type) 
    VALUES('${name}', '${parameter_type}', '${type}')
    RETURNING *`;

    const result = await pool.query(createParameterQuery);

    return result.rows && result.rows[0];
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({ type: 'db-error', source: 'create-parameter', data: error.message })
    );
  }
};

export default createSpecParameter;
