import { get_product } from "../../../lib/helpers.js/get_product";
import populateSpecsForParams from "../../../lib/populateSpecsForParams";
import getDeviations from "../../../lib/getDeviations";
import sendMail from "../../../lib/sendMail";
import prepareDeviationEmail from "../../../lib/deviation_template";

const CreateProcessInspectionRecord = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;
    await pool.query("BEGIN");

    const {
      type,
      product,
      size,
      date,
      shift,
      comment,
      extra_fields,
    } = args.data;

    // TODO select the table we want to store values
    const product_data = await get_product(pool, product);
    let tableName = `${product_data.plant}_${type}`.toLowerCase();

    // format inputs
    const comment_formatted = comment !== undefined ? `'${comment}', ` : "";

    // extra field holder
    let extra_field_keys = "";
    let extra_field_values = "";

    extra_fields.forEach((field) => {
      if (field.type == "TEXT") {
        extra_field_keys += `, ${field.parameter}`;
        extra_field_values += `, '${field.text_value}'`;
      } else if (field.type == "BOOLEAN") {
        extra_field_keys += `, ${field.parameter}`;
        extra_field_values += `, ${field.boolean_value}`;
      } else if (field.type == "NUMBER") {
        extra_field_keys += `, ${field.parameter}`;
        extra_field_values += `, ${field.numeric_value}`;
      } else if (field.type == "DATE") {
        extra_field_keys += `, ${field.parameter}`;
        extra_field_values += `, '${field.text_value}'`; // date params needs a wrap
      }
    });

    // get plant parameters with specs
    const plantParameters = await populateSpecsForParams(
      {
        plant: product_data.plant,
        product: product,
        size: size,
        spec_type: type,
      },
      pool
    );

    // handle deviations section
    let is_deviated = false;
    const deviations = getDeviations(extra_fields, plantParameters, ctx);

    console.log(33333, deviations);

    if (deviations.length > 0) {
      is_deviated = true;
    }

    console.log(111111, "so far so good");
    // remove date, image type params
    plantParameters = plantParameters.filter((parameter) => {
      return !["IMAGE", "DATE", "TEXT"].includes(parameter.parameter_type);
    });

    const queryText =
      `INSERT INTO ${tableName}(` +
      `user_id, ` +
      `size, ` +
      `product, ` +
      `date, ` +
      `is_deviated, ` +
      `${comment != undefined ? "comment, " : ""}` + // auto generator will add the comma
      `shift` +
      extra_field_keys +
      `) VALUES(` +
      `'${ctx.user.id}',` +
      `'${size}',` +
      `'${product}',` +
      `'${date}',` +
      `'${is_deviated}',` +
      `${comment_formatted}` +
      `'${shift}'` +
      extra_field_values + // auto generator will add the comma
      `) RETURNING *`;

    // remove the last comma before the closing bracket
    queryText = queryText.replace(/\,\)/g, ")");

    const inspectionRecordResult = await pool.query(queryText);
    const record_id = inspectionRecordResult.rows[0].id;

    if (is_deviated) {
      const deviationsString = ``;

      deviations.forEach((deviation) => {
        deviationsString =
          deviationsString +
          `(${deviation.value}, '${deviation.specification}', ${deviation.created_by}, ${deviation.plant_parameter}, ${record_id}, '${product_data.plant}', ${product}, ${size} ),`;
      });
      // remove last comma from the deviationsString
      deviationsString = deviationsString.replace(/,\s*$/, "");

      const insertDeviationsQuery = `INSERT INTO deviation (value, specification, created_by, plant_parameter, record_id, plant, product, size) VALUES ${deviationsString} RETURNING *`;
      await pool.query(insertDeviationsQuery);
    }

    // fetch the product size to get it name
    const sizeQuery = `SELECT * FROM "size" where id='${size}'`;
    let sizeResult = await pool.query(sizeQuery);
    sizeResult = sizeResult.rows[0];

    // fetch the authorized users email addresses
    const usersToNotifyQuery = `SELECT email FROM "user" where role IN ('QA_MANAGER', 'PRODUCTION_MANAGER')`;
    let usersToNotifyResult = await pool.query(usersToNotifyQuery);
    usersToNotifyResult = usersToNotifyResult.rows;

    console.log("so far so good 4");

    // prepare the deviation section to  embeded to the email
    const deviationDataForTemplate = deviations.map((deviation) => {
      console.log(320320, deviation);

      console.log(
        252525,
        typeof deviation.specification,
        deviation.specification
      );
      const specification = "";
      if (deviation.specification.includes(",")) {
        specification = `min: ${deviation.specification.split(",")[0]} , max :${
          deviation.specification.split(",")[1]
        }`;
      } else if (
        deviation.specification === "true" ||
        deviation.specification === "false"
      ) {
        specification = deviation.specification === "true" ? "Ok" : "Not Ok";
      } else {
        specification = deviation.specification;
      }

      return {
        value: deviation.value,
        parameter: plantParameters.find(
          (parameter) => parameter.id === deviation.plant_parameter
        ).parameter,
        specification: specification,
      };
    });

    // prepare the record url to embded to the email
    const record_link = `${process.env.ADMIN_APP_URL}/${
      type === "PROCESS" ? "process-data" : "packaging-data"
    }/${product_data.plant}/${record_id}`;

    // prepare the email
    const email = prepareDeviationEmail({
      deviations: deviationDataForTemplate,
      product: product_data.name,
      plant: product_data.plant,
      size: sizeResult.size_name,
      record_link,
    });

    console.log(300123, email);

    const emailalertsEnabled =
      process.env.ALERT_EMAIL_ON_DEFECTS === "true" ? true : false;

    console.log(
      456,
      emailalertsEnabled,
      email,
      usersToNotifyResult.map((user) => user.email)
    );
    if (emailalertsEnabled) {
      await sendMail({
        emailFrom: '"management 🤵‍ " <management@chandimasweets.online>',
        emailTo: usersToNotifyResult.map((user) => user.email),
        htmlPayload: email,
        subject: "Product Quality Deviation Notification",
        textPayload: "please use an html supported browser",
      });
    }

    await pool.query("COMMIT");

    return {
      status: "success",
      values: "no values actually",
    };
  } catch (error) {
    await pool.query("ROLLBACK");
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "create-inspection-record",
        data: error.message,
      })
    );
  }
};

export default CreateProcessInspectionRecord;
