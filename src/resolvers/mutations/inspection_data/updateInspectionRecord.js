const updatePlant = async (parent, args, ctx, info) => {
  try {
    const pool = ctx.postgresPool;

    const { record_type, plant, recordId } = args.where;

    console.log(args.where);

    // now determine the plant
    let tableName = `${plant}_${record_type}`.toLowerCase();

    const constUpdateQuery = `UPDATE "${tableName}" SET approved_by = ${args.data.approved_by} WHERE id = ${recordId} RETURNING *`;
    const result = await pool.query(constUpdateQuery);

    return {
      status: "ok",
      message: "update succesful",
    };

    console.log(5555, constUpdateQuery);

    console.log(result.rows);
  } catch (error) {
    console.log(error);
    return new Error(
      JSON.stringify({
        type: "db-error",
        source: "update-ncr",
        data: error.message,
      })
    );
  }
};

export default updatePlant;
