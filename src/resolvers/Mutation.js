import deletePlant from "./mutations/plant/deletePlant";
import createPlant from "./mutations/plant/createPlant";
import updatePlant from "./mutations/plant/updatePlant";

import createUser from "./mutations/user/createUser";
import updateUser from "./mutations/user/updateUser";

import createSpecParameter from "./mutations/spec_parameter/createSpecParameter";
import deleteSpecParameter from "./mutations/spec_parameter/deleteSpecParameter";
import updateSpecParameter from "./mutations/spec_parameter/updateSpecParameter";

import createProduct from "./mutations/product/createProduct";
import deleteProduct from "./mutations/product/deleteProduct";
import updateProduct from "./mutations/product/updateProduct";

import updatePlantParameter from "./mutations/plant_parameter/updatePlantParameter";
import createPlantParameter from "./mutations/plant_parameter/createPlantParameter";
import deletePlantParameter from "./mutations/plant_parameter/deletePlantParameter";
import login from "./mutations/user/login";
import logout from "./mutations/user/logout";

import createBackup from "./mutations/backup/createBackup";
import restoreBackup from "./mutations/backup/restoreBackup";

import createSize from "./mutations/size/createSize";
import updateSize from "./mutations/size/updateSize";

import createUserLog from "./mutations/log/createLog";

import requestPasswordReset from "./mutations/user/requestPasswordReset";
import resetPassword from "./mutations/user/resetPassword";

import createNewInspectionRecord from "./mutations/inspection_data/createNewInspectionRecord";
import updateInspectionRecord from "./mutations/inspection_data/updateInspectionRecord";

import updateDeviation from "./mutations/deviations/updateDeviation";
import setDeviation from "./mutations/deviations/setDeviation";

import createSpecification from "./mutations/specification/createSpecification";
import updateSpecification from "./mutations/specification/updateSpecification";

import createNCR from "./mutations/ncr/createNCR";
import updateNCR from "./mutations/ncr/updateNCR";

const Mutations = {
  createNCR,
  updateNCR,

  resetPassword,
  requestPasswordReset,

  updateDeviation,
  setDeviation,

  createNewInspectionRecord,
  updateInspectionRecord,

  createUserLog,

  createSpecParameter,
  deleteSpecParameter,
  updateSpecParameter,

  createSpecification,
  updateSpecification,

  createUser,
  login,
  logout,
  updateUser,

  createPlant,
  deletePlant,
  updatePlant,

  createProduct,
  deleteProduct,
  updateProduct,

  createPlantParameter,
  updatePlantParameter,
  deletePlantParameter,

  createBackup,
  restoreBackup,

  createSize,
  updateSize,
};

export default Mutations;
