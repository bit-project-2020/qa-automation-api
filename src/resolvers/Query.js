import plants from "./queries/plant/plants.js";
import specParameters from "./queries/spec_parameter/specParameters";

import currentUser from "./queries/user/currentUser";
import users from "./queries/user/users";
import user from "./queries/user/user";

import product from "./queries/product/product";
import products from "./queries/product/products";
import productSpecifications from "./queries/product/productSpecifications";

import specifications from "./queries/specifications/specfications";

import plantParameters from "./queries/plant_parameter/plant_parameters";

import units from "./queries/unit/units";

import sizes from "./queries/size/sizes";
import size from "./queries/size/size";

import logs from "./queries/log/logs";

import deviations from "./queries/deviations/deviations";
import specificationDeviations from "./queries/deviations/specificationDeviations";

import backups from "./queries/backup/backups";

import inspectionData from "./queries/inspectionData/inspection_data";
import inspectionDataRecord from "./queries/inspectionData/inspection_data_one";

import ncrs from "./queries/ncr/ncrs";

import report_deviation from "./queries/reports/reportDeviation";
import ncr_report from "./queries/reports/ncrReport";

const Query = {
  report_deviation,
  ncr_report,

  ncrs,

  inspectionData,

  inspectionDataRecord,

  backups,

  deviations,
  specificationDeviations,

  logs,

  specParameters,
  plants,

  plantParameters,

  currentUser,
  users,
  user,

  product,
  products,
  productSpecifications,

  specifications,

  units,

  size,
  sizes,
};

export default Query;
