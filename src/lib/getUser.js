import jwt from "jsonwebtoken";

const getUser = async ({ postgresPool, req }) => {
  try {
    const { admin_token, token, user_token } = req.request.cookies;

    const appname = req.request.headers.appname;

    let selectedToken = token;

    if (appname === "Admin" && admin_token) {
      selectedToken = admin_token;
    } else if (appname === "User" && user_token) {
      selectedToken = user_token;
    } else if (token) {
      selectedToken = token;
    } else {
      return null;
    }

    // decrypt the token and get the id
    const { id } = await jwt.verify(selectedToken, process.env.APP_SECRET);

    const userQuery = `SELECT 
      id, 
      first_name, 
      last_name, 
      role,
      email,
      profile_photo,
      department,
      user_name,
      emp_no
    FROM "user" WHERE id=${id}`;

    const result = await postgresPool.query(userQuery);

    return result.rows && result.rows[0] != [] ? result.rows[0] : null;
  } catch (error) {
    // console.log(error);
  }
};

export default getUser;
