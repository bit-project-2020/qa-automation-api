const getDeviations = (extra_fields, plantParameters, ctx) => {
  const deviations = [];

  extra_fields.forEach((extraField) => {
    console.log(301301, extraField);
    const relaventParam = plantParameters.find((parameter) => {
      return parameter.parameter == extraField.parameter;
    });

    if (relaventParam && relaventParam.specification) {
      // if the param is a numeric param
      if (relaventParam.parameter_type === "NUMBER") {
        console.log(555333, relaventParam);

        // comma seperate min max
        const [min, max] = relaventParam.specification.split(",");
        if (
          (extraField.numeric_value != null &&
            extraField.numeric_value < min) ||
          extraField.numeric_value > max
        ) {
          // deviated, push deviation to the deviation object
          const newDeviation = {
            value: extraField.numeric_value,
            specification: relaventParam.specification,
            created_by: ctx.user.id,
            plant_parameter: relaventParam.id,
            // record_id: record_id
          };

          deviations.push(newDeviation);
        }
      }

      // if the param is a boolean param
      if (relaventParam.parameter_type === "BOOLEAN") {
        // cast string type to boolean
        let specCastedValue;

        if (relaventParam.specification != undefined) {
          specCastedValue =
            relaventParam.specification === "true" ? true : false;
        }

        if (
          extraField.boolean_value !== specCastedValue &&
          specCastedValue != undefined
        ) {
          // deviated, push deviation to the deviation object
          const newDeviation = {
            value: extraField.boolean_value,
            specification: relaventParam.specification,
            created_by: ctx.user.id,
            plant_parameter: relaventParam.id,
            // record_id: record_id
          };

          deviations.push(newDeviation);
        }
      }

      // if the param is numeric exact param
      if (relaventParam.parameter_type === "NUMBER_EXACT") {
        console.log(66666666, "exact value parameter found");

        // cast string type to boolean
        if (relaventParam.specification != undefined) {
          if (
            Math.abs(
              extraField.numeric_value - parseFloat(relaventParam.specification)
            ) > 0.001
          ) {
            // deviated
            const newDeviation = {
              value: extraField.numeric_value,
              specification: relaventParam.specification,
              created_by: ctx.user.id,
              plant_parameter: relaventParam.id,
              // record_id: record_id
            };
            deviations.push(newDeviation);
          }
        }
      }
    }
    // console.log(400,relaventParam, extraField)
  });

  return deviations;
};

export default getDeviations;
