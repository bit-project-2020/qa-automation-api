const prepareDeviationEmail = ({
  plant,
  product,
  size,
  deviations,
  record_link,
}) => {
  const deviated_rows = "";

  deviations.forEach((deviation) => {
    // copying the value to avoid parameter reasign

    let deviationvaluecopy = deviation.value;
    if (deviationvaluecopy === true || deviationvaluecopy === false) {
      deviationvaluecopy = deviationvaluecopy === true ? "Ok" : "Not Ok";
      console.log(334444, deviationvaluecopy);
    }

    deviated_rows = `${deviated_rows} 
    <tr style="height: 18px;">
    <td style="width: 26.6666%; height: 18px;">${deviation.parameter} </td>
    <td style="width: 17.6042%; height: 18px;">${deviationvaluecopy}</td>
    <td style="width: 17.6042%; height: 18px;">${deviation.specification}</td>
    </tr>
    `;
  });

  return `<p>There is/are recent parameter deviations observed in the <strong> ${plant} plant</strong></p>
  <p>Product: ${product}</p>
  <p>Size: ${size}</p>
  <table style="border-collapse: collapse; width: 61.875%; height: 54px;" border="1">
  <tbody>
  <tr style="height: 18px;">
  <td style="width: 26.6666%; height: 18px;"><strong>Parameter</strong></td>
  <td style="width: 17.6042%; height: 18px;"><strong>Value</strong></td>
  <td style="width: 17.6042%; height: 18px;"><strong>specification</strong></td>
  </tr>
  ${deviated_rows}
  </tbody>
  </table>
  <p>please click on<a href="${record_link}"> this link</a> to view in detail.</p>`;
};

export default prepareDeviationEmail;
