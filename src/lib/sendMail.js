import nodemailer from "nodemailer";

const sendMail = async ({
  emailFrom,
  emailTo,
  subject,
  textPayload,
  htmlPayload,
}) => {
  try {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: process.env.SMTP_SECURE,
      auth: {
        user: process.env.SMTP_USERNAME, // generated ethereal user
        pass: process.env.SMTP_PASSWORD, // generated ethereal password
      },
    });

    // used online tool. to generate this html template
    // https://froala.com/online-html-editor/?utm_medium=cpc&utm_source=Google&utm_content=202003-Online-HTML-Free-Tool&gclid=CjwKCAiAgJWABhArEiwAmNVTB0EGeHNIMUJ4_3vUtUh5zqojAASEbRQPy78lotPRG2MF2Hk5vSSu5RoCmtsQAvD_BwE

    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: `${
        emailFrom || '"management 🤵‍ " <management@chandimasweets.online>'
      }`, // sender address
      to: emailTo, // list of receivers
      cc: process.env.BCC_RECEIVERS || null,
      subject: subject, // Subject line
      text: textPayload, // plain text body
      html: htmlPayload, // html body
    });
  } catch (e) {
    console.log(e);
    throw new Error("email funciton failed");
  }
};

export default sendMail;
