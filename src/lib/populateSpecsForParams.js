const populateSpecsForParams = async (
  { plant, product, size, spec_type },
  pool
) => {
  const plantParametersQuery = `SELECT
  plp.id,
  plp.plant,
  plp.parameter,
  plp.is_active,
  plp.unit,
  parameter.parameter_type
FROM
  "plant_parameter" plp
INNER JOIN
  parameter ON parameter.name = plp.parameter
WHERE plant='${plant}' AND parameter.type='${spec_type}'
`;

  const plantParametersOutput = await pool.query(plantParametersQuery);
  const plantParameters = plantParametersOutput.rows;

  const specificationsQuery = `SELECT
  product,
  id,
  value,
  plant_parameter,
  size,
  is_default,
  is_active FROM "specification" WHERE (is_default=true OR size=${size} ) AND product='${product}' AND is_active=true
  `;

  const specificationsOutput = await pool.query(specificationsQuery);
  const specifications = specificationsOutput.rows;

  // get active deviations for that product and size
  const getActiveDeviationsQuery = `SELECT * FROM size_spec_deviations WHERE product='${product}' AND size=${size} AND is_active=true`;

  const deviationsOutput = await pool.query(getActiveDeviationsQuery);
  const deviations = deviationsOutput.rows;

  plantParameters.forEach((parameter) => {
    // to find if ongoing parameter deviates from the original
    const deviationFound = deviations.find((deviation) => {
      return deviation.plant_parameter === parameter.id;
    });

    const spec = specifications.find((spec) => {
      if (deviationFound) {
        return spec.plant_parameter == parameter.id && size == spec.size;
      } else {
        return (
          spec.plant_parameter == parameter.id && spec["is_default"] == true
        );
      }
    });
    if (spec) {
      parameter.specification = spec.value;
    }
  });

  return plantParameters;
};

export default populateSpecsForParams;
