import { rule, shield, and, or, not, allow } from "graphql-shield"

// define how rules should work
const isAuthenticated = rule()(
  async (parent, args, ctx, info) => ctx.user != null
)
const isSystemAdmin = rule()(
  async (parent, args, ctx, info) => ctx.user.role == "SYSTEM_ADMIN"
)
const isProductionManager = rule()(
  async (parent, args, ctx, info) => ctx.user.role == "QA_MANAGER"
)
const isQAO = rule()(
  async (parent, args, ctx, info) => ctx.user.role == "QA_OFFICER"
)
const isQAE = rule()(
  async (parent, args, ctx, info) => ctx.user.role == "QA_EXECUTIVE"
)
const isQAM = rule()(
  async (parent, args, ctx, info) => ctx.user.role == "QA_MANAGER"
)

// note: extra security rules can be imposed in the resolver function of the each query/mutation.

const permissions = shield({
  // set access levels for graphql queries
  Query: {
    plants: allow,
    currentUser: allow,
    logs: isAuthenticated,
    productSpecifications: isAuthenticated,
    specParameters: isAuthenticated,
    user: isAuthenticated,
    users: allow,
    product: isAuthenticated,
    products: isAuthenticated,
    productSpecifications: isAuthenticated,
    plantParameters: isAuthenticated,
    sizes: isAuthenticated,
    units: isAuthenticated,
    backups: isSystemAdmin,
    "*": allow,
    // productData: isAuthenticated,
    // documents: isAuthenticated,
  },
  // set access levels for graphql mutations
  Mutation: {
    requestPasswordReset: allow,
    login: allow,
    logout: allow,

    createUserLog: or(isQAO, isSystemAdmin),
    // updateUserLog: isQAO,

    updatePlantParameter: and(isAuthenticated, or(isQAM, isQAE, isSystemAdmin)),
    createPlantParameter: and(isAuthenticated, or(isQAM, isQAE, isSystemAdmin)),
    deletePlantParameter: and(isAuthenticated, or(isQAM, isQAE, isSystemAdmin)),

    createSpecParameter: isAuthenticated,
    deleteSpecParameter: isAuthenticated,

    // createSpecification: isQAM,
    // updateSpecification: isQAM,
    // deleteSpecification: isQAM,

    createUser: isSystemAdmin,
    deleteUser: isSystemAdmin,
    updateUser: allow, // only system admin should do

    createPlant: or(isProductionManager, isSystemAdmin),
    // updaetPlant: isProductionManager,
    deletePlant: isSystemAdmin,

    createProduct: isAuthenticated,
    deleteProduct: isAuthenticated,
    updateProduct: isAuthenticated,

     createBackup: isSystemAdmin,
    //  deleteBackup: isSystemAdmin,
    // restoreBackup: isSystemAdmin,
    restoreBackup: allow,

    createSize: or(isProductionManager , isSystemAdmin),
    updateSize: or(isProductionManager , isSystemAdmin),

    "*": allow,
  },
})

export default permissions
