PGDMP     3                    y            postgres    12.3 (Debian 12.3-1.pgdg100+1) #   12.6 (Ubuntu 12.6-0ubuntu0.20.04.1) �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    13408    postgres    DATABASE     x   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
    DROP DATABASE postgres;
                postgres    false            �           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    3205            �            1259    107537    biscuit_packaging    TABLE     _  CREATE TABLE public.biscuit_packaging (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL,
    is_deviated boolean DEFAULT false,
    price double precision
);
 %   DROP TABLE public.biscuit_packaging;
       public         heap    postgres    false            �            1259    107545    biscuit_packaging_id_seq    SEQUENCE     �   CREATE SEQUENCE public.biscuit_packaging_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.biscuit_packaging_id_seq;
       public          postgres    false    202            �           0    0    biscuit_packaging_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.biscuit_packaging_id_seq OWNED BY public.biscuit_packaging.id;
          public          postgres    false    203            �            1259    107547    biscuit_process    TABLE     �  CREATE TABLE public.biscuit_process (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL,
    odour boolean,
    moisture double precision,
    thinkness double precision,
    front_view character varying,
    is_deviated boolean DEFAULT false
);
 #   DROP TABLE public.biscuit_process;
       public         heap    postgres    false            �            1259    107555    biscuit_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.biscuit_process_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.biscuit_process_id_seq;
       public          postgres    false    204            �           0    0    biscuit_process_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.biscuit_process_id_seq OWNED BY public.biscuit_process.id;
          public          postgres    false    205            �            1259    107557    cake_packaging    TABLE     @  CREATE TABLE public.cake_packaging (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL,
    is_deviated boolean DEFAULT false
);
 "   DROP TABLE public.cake_packaging;
       public         heap    postgres    false            �            1259    107565    cake_packaging_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cake_packaging_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.cake_packaging_id_seq;
       public          postgres    false    206            �           0    0    cake_packaging_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.cake_packaging_id_seq OWNED BY public.cake_packaging.id;
          public          postgres    false    207            �            1259    107567    cake_process    TABLE     ]  CREATE TABLE public.cake_process (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL,
    moisture double precision,
    is_deviated boolean DEFAULT false
);
     DROP TABLE public.cake_process;
       public         heap    postgres    false            �            1259    107575    cake_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cake_process_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cake_process_id_seq;
       public          postgres    false    208            �           0    0    cake_process_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cake_process_id_seq OWNED BY public.cake_process.id;
          public          postgres    false    209            �            1259    107577 
   department    TABLE     k   CREATE TABLE public.department (
    name character varying NOT NULL,
    description character varying
);
    DROP TABLE public.department;
       public         heap    postgres    false            �            1259    107583 	   deviation    TABLE     �  CREATE TABLE public.deviation (
    id integer NOT NULL,
    value character varying NOT NULL,
    specification character varying NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    plant_parameter integer NOT NULL,
    record_id integer NOT NULL,
    plant character varying NOT NULL,
    product integer NOT NULL,
    size integer NOT NULL
);
    DROP TABLE public.deviation;
       public         heap    postgres    false            �            1259    107590    deviation_id_seq    SEQUENCE     �   CREATE SEQUENCE public.deviation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.deviation_id_seq;
       public          postgres    false    211            �           0    0    deviation_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.deviation_id_seq OWNED BY public.deviation.id;
          public          postgres    false    212            �            1259    107592    machine    TABLE     �   CREATE TABLE public.machine (
    name character varying NOT NULL,
    machine_number character varying NOT NULL,
    is_active character varying DEFAULT true NOT NULL,
    plant character varying
);
    DROP TABLE public.machine;
       public         heap    postgres    false            �            1259    107599    ncr_data    TABLE     �  CREATE TABLE public.ncr_data (
    id integer NOT NULL,
    created_by integer,
    qa_representative integer,
    deviated_parameters character varying,
    reason character varying,
    production_representative integer,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    plant character varying NOT NULL,
    product integer NOT NULL,
    is_active boolean DEFAULT true,
    size integer NOT NULL,
    type character varying NOT NULL,
    comment character varying,
    record_id integer NOT NULL,
    CONSTRAINT ncr_data_check CHECK (((type)::text = ANY (ARRAY[('PACKAGING'::character varying)::text, ('PROCESS'::character varying)::text])))
);
    DROP TABLE public.ncr_data;
       public         heap    postgres    false            �           0    0    COLUMN ncr_data.reason    COMMENT     R   COMMENT ON COLUMN public.ncr_data.reason IS 'to be filled by production manager';
          public          postgres    false    214            �           0    0 )   COLUMN ncr_data.production_representative    COMMENT     n   COMMENT ON COLUMN public.ncr_data.production_representative IS 'to be filled by relavent production manager';
          public          postgres    false    214            �            1259    107608    ncr_data_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ncr_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.ncr_data_id_seq;
       public          postgres    false    214            �           0    0    ncr_data_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.ncr_data_id_seq OWNED BY public.ncr_data.id;
          public          postgres    false    215            �            1259    107610 	   parameter    TABLE     �  CREATE TABLE public.parameter (
    name character varying NOT NULL,
    parameter_type character varying NOT NULL,
    type character varying NOT NULL,
    is_active boolean DEFAULT true,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    CONSTRAINT spec_parameter_type_check CHECK (((type)::text = ANY (ARRAY[('PACKAGING'::character varying)::text, ('PROCESS'::character varying)::text])))
);
    DROP TABLE public.parameter;
       public         heap    postgres    false            �            1259    107619    parameter_type    TABLE     t   CREATE TABLE public.parameter_type (
    type_name character varying NOT NULL,
    description character varying
);
 "   DROP TABLE public.parameter_type;
       public         heap    postgres    false            �           0    0    TABLE parameter_type    COMMENT     8   COMMENT ON TABLE public.parameter_type IS 'Enum Table';
          public          postgres    false    217            �            1259    107625    plant    TABLE     �   CREATE TABLE public.plant (
    plant_name character varying NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    image_url character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.plant;
       public         heap    postgres    false            �            1259    107633    plant_parameter    TABLE        CREATE TABLE public.plant_parameter (
    id integer NOT NULL,
    plant character varying NOT NULL,
    parameter character varying NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    unit character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
 #   DROP TABLE public.plant_parameter;
       public         heap    postgres    false            �            1259    107641    plant_parameters_id_seq    SEQUENCE     �   CREATE SEQUENCE public.plant_parameters_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.plant_parameters_id_seq;
       public          postgres    false    219            �           0    0    plant_parameters_id_seq    SEQUENCE OWNED BY     R   ALTER SEQUENCE public.plant_parameters_id_seq OWNED BY public.plant_parameter.id;
          public          postgres    false    220            �            1259    107643    product    TABLE       CREATE TABLE public.product (
    plant character varying NOT NULL,
    name character varying NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    id integer NOT NULL,
    image_url character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.product;
       public         heap    postgres    false            �            1259    107651    product_column1_seq    SEQUENCE     �   CREATE SEQUENCE public.product_column1_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.product_column1_seq;
       public          postgres    false    221            �           0    0    product_column1_seq    SEQUENCE OWNED BY     F   ALTER SEQUENCE public.product_column1_seq OWNED BY public.product.id;
          public          postgres    false    222            �            1259    107653    shift    TABLE     �   CREATE TABLE public.shift (
    name character varying NOT NULL,
    start_time time without time zone,
    end_time time without time zone
);
    DROP TABLE public.shift;
       public         heap    postgres    false            �            1259    107659    size    TABLE     �   CREATE TABLE public.size (
    id integer NOT NULL,
    size_name character varying NOT NULL,
    product integer NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now()
);
    DROP TABLE public.size;
       public         heap    postgres    false            �           0    0    COLUMN size.size_name    COMMENT     J   COMMENT ON COLUMN public.size.size_name IS '5g, 10g or medium large etc';
          public          postgres    false    224            �            1259    107667    size_id_seq    SEQUENCE     �   CREATE SEQUENCE public.size_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.size_id_seq;
       public          postgres    false    224            �           0    0    size_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.size_id_seq OWNED BY public.size.id;
          public          postgres    false    225            �            1259    107669    size_spec_deviations    TABLE       CREATE TABLE public.size_spec_deviations (
    product integer NOT NULL,
    plant_parameter integer NOT NULL,
    size integer NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    id integer NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
 (   DROP TABLE public.size_spec_deviations;
       public         heap    postgres    false            �            1259    107674    size_spec_deviations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.size_spec_deviations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.size_spec_deviations_id_seq;
       public          postgres    false    226            �           0    0    size_spec_deviations_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.size_spec_deviations_id_seq OWNED BY public.size_spec_deviations.id;
          public          postgres    false    227            �            1259    107676    specification    TABLE     J  CREATE TABLE public.specification (
    is_active boolean DEFAULT true NOT NULL,
    product integer NOT NULL,
    id integer NOT NULL,
    value character varying,
    plant_parameter integer NOT NULL,
    is_default boolean DEFAULT true,
    size integer,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
 !   DROP TABLE public.specification;
       public         heap    postgres    false            �            1259    107685    specification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.specification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.specification_id_seq;
       public          postgres    false    228            �           0    0    specification_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.specification_id_seq OWNED BY public.specification.id;
          public          postgres    false    229            �            1259    107687    unit    TABLE     e   CREATE TABLE public.unit (
    unit character varying NOT NULL,
    description character varying
);
    DROP TABLE public.unit;
       public         heap    postgres    false            �            1259    107693    user    TABLE        CREATE TABLE public."user" (
    first_name character varying,
    last_name character varying,
    role character varying NOT NULL,
    department character varying,
    user_name character varying,
    password character varying NOT NULL,
    emp_no character varying,
    id integer NOT NULL,
    email character varying,
    is_active boolean DEFAULT true,
    otp character varying,
    otp_expiry timestamp(0) without time zone,
    profile_photo character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public."user";
       public         heap    postgres    false            �            1259    107701    user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.user_id_seq;
       public          postgres    false    231            �           0    0    user_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;
          public          postgres    false    232            �            1259    107703    user_log    TABLE       CREATE TABLE public.user_log (
    id integer NOT NULL,
    plant character varying NOT NULL,
    created_by integer NOT NULL,
    log text,
    shift text NOT NULL,
    title character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.user_log;
       public         heap    postgres    false            �            1259    107710    user_log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.user_log_id_seq;
       public          postgres    false    233            �           0    0    user_log_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.user_log_id_seq OWNED BY public.user_log.id;
          public          postgres    false    234            �            1259    107712 	   user_role    TABLE     s   CREATE TABLE public.user_role (
    description character varying(256),
    role character varying(32) NOT NULL
);
    DROP TABLE public.user_role;
       public         heap    postgres    false            V           2604    107735    biscuit_packaging id    DEFAULT     |   ALTER TABLE ONLY public.biscuit_packaging ALTER COLUMN id SET DEFAULT nextval('public.biscuit_packaging_id_seq'::regclass);
 C   ALTER TABLE public.biscuit_packaging ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202            Y           2604    107736    biscuit_process id    DEFAULT     x   ALTER TABLE ONLY public.biscuit_process ALTER COLUMN id SET DEFAULT nextval('public.biscuit_process_id_seq'::regclass);
 A   ALTER TABLE public.biscuit_process ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204            \           2604    107737    cake_packaging id    DEFAULT     v   ALTER TABLE ONLY public.cake_packaging ALTER COLUMN id SET DEFAULT nextval('public.cake_packaging_id_seq'::regclass);
 @   ALTER TABLE public.cake_packaging ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206            _           2604    107738    cake_process id    DEFAULT     r   ALTER TABLE ONLY public.cake_process ALTER COLUMN id SET DEFAULT nextval('public.cake_process_id_seq'::regclass);
 >   ALTER TABLE public.cake_process ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208            a           2604    107739    deviation id    DEFAULT     l   ALTER TABLE ONLY public.deviation ALTER COLUMN id SET DEFAULT nextval('public.deviation_id_seq'::regclass);
 ;   ALTER TABLE public.deviation ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    211            e           2604    107740    ncr_data id    DEFAULT     j   ALTER TABLE ONLY public.ncr_data ALTER COLUMN id SET DEFAULT nextval('public.ncr_data_id_seq'::regclass);
 :   ALTER TABLE public.ncr_data ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214            n           2604    107741    plant_parameter id    DEFAULT     y   ALTER TABLE ONLY public.plant_parameter ALTER COLUMN id SET DEFAULT nextval('public.plant_parameters_id_seq'::regclass);
 A   ALTER TABLE public.plant_parameter ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    219            q           2604    107742 
   product id    DEFAULT     m   ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_column1_seq'::regclass);
 9   ALTER TABLE public.product ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    221            t           2604    107743    size id    DEFAULT     b   ALTER TABLE ONLY public.size ALTER COLUMN id SET DEFAULT nextval('public.size_id_seq'::regclass);
 6   ALTER TABLE public.size ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    225    224            w           2604    107744    size_spec_deviations id    DEFAULT     �   ALTER TABLE ONLY public.size_spec_deviations ALTER COLUMN id SET DEFAULT nextval('public.size_spec_deviations_id_seq'::regclass);
 F   ALTER TABLE public.size_spec_deviations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226            {           2604    107745    specification id    DEFAULT     t   ALTER TABLE ONLY public.specification ALTER COLUMN id SET DEFAULT nextval('public.specification_id_seq'::regclass);
 ?   ALTER TABLE public.specification ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    229    228            ~           2604    107746    user id    DEFAULT     d   ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);
 8   ALTER TABLE public."user" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    232    231            �           2604    107747    user_log id    DEFAULT     j   ALTER TABLE ONLY public.user_log ALTER COLUMN id SET DEFAULT nextval('public.user_log_id_seq'::regclass);
 :   ALTER TABLE public.user_log ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    234    233            ^          0    107537    biscuit_packaging 
   TABLE DATA           q   COPY public.biscuit_packaging (id, user_id, size, product, shift, comment, date, is_deviated, price) FROM stdin;
    public          postgres    false    202   ��       `          0    107547    biscuit_process 
   TABLE DATA           �   COPY public.biscuit_process (id, user_id, size, product, shift, comment, date, odour, moisture, thinkness, front_view, is_deviated) FROM stdin;
    public          postgres    false    204   ��       b          0    107557    cake_packaging 
   TABLE DATA           g   COPY public.cake_packaging (id, user_id, size, product, shift, comment, date, is_deviated) FROM stdin;
    public          postgres    false    206   %�       d          0    107567    cake_process 
   TABLE DATA           o   COPY public.cake_process (id, user_id, size, product, shift, comment, date, moisture, is_deviated) FROM stdin;
    public          postgres    false    208   B�       f          0    107577 
   department 
   TABLE DATA           7   COPY public.department (name, description) FROM stdin;
    public          postgres    false    210   _�       g          0    107583 	   deviation 
   TABLE DATA           �   COPY public.deviation (id, value, specification, created_by, created_at, plant_parameter, record_id, plant, product, size) FROM stdin;
    public          postgres    false    211   ��       i          0    107592    machine 
   TABLE DATA           I   COPY public.machine (name, machine_number, is_active, plant) FROM stdin;
    public          postgres    false    213   
�       j          0    107599    ncr_data 
   TABLE DATA           �   COPY public.ncr_data (id, created_by, qa_representative, deviated_parameters, reason, production_representative, created_at, plant, product, is_active, size, type, comment, record_id) FROM stdin;
    public          postgres    false    214   '�       l          0    107610 	   parameter 
   TABLE DATA           V   COPY public.parameter (name, parameter_type, type, is_active, created_at) FROM stdin;
    public          postgres    false    216   {�       m          0    107619    parameter_type 
   TABLE DATA           @   COPY public.parameter_type (type_name, description) FROM stdin;
    public          postgres    false    217   &�       n          0    107625    plant 
   TABLE DATA           M   COPY public.plant (plant_name, is_active, image_url, created_at) FROM stdin;
    public          postgres    false    218   ��       o          0    107633    plant_parameter 
   TABLE DATA           \   COPY public.plant_parameter (id, plant, parameter, is_active, unit, created_at) FROM stdin;
    public          postgres    false    219   %�       q          0    107643    product 
   TABLE DATA           T   COPY public.product (plant, name, is_active, id, image_url, created_at) FROM stdin;
    public          postgres    false    221   ��       s          0    107653    shift 
   TABLE DATA           ;   COPY public.shift (name, start_time, end_time) FROM stdin;
    public          postgres    false    223   ��       t          0    107659    size 
   TABLE DATA           M   COPY public.size (id, size_name, product, is_active, created_at) FROM stdin;
    public          postgres    false    224   ��       v          0    107669    size_spec_deviations 
   TABLE DATA           i   COPY public.size_spec_deviations (product, plant_parameter, size, is_active, id, created_at) FROM stdin;
    public          postgres    false    226   ��       x          0    107676    specification 
   TABLE DATA           u   COPY public.specification (is_active, product, id, value, plant_parameter, is_default, size, created_at) FROM stdin;
    public          postgres    false    228   �       z          0    107687    unit 
   TABLE DATA           1   COPY public.unit (unit, description) FROM stdin;
    public          postgres    false    230   ��       {          0    107693    user 
   TABLE DATA           �   COPY public."user" (first_name, last_name, role, department, user_name, password, emp_no, id, email, is_active, otp, otp_expiry, profile_photo, created_at) FROM stdin;
    public          postgres    false    231   T�       }          0    107703    user_log 
   TABLE DATA           X   COPY public.user_log (id, plant, created_by, log, shift, title, created_at) FROM stdin;
    public          postgres    false    233   ��                 0    107712 	   user_role 
   TABLE DATA           6   COPY public.user_role (description, role) FROM stdin;
    public          postgres    false    235   ��       �           0    0    biscuit_packaging_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.biscuit_packaging_id_seq', 1, false);
          public          postgres    false    203            �           0    0    biscuit_process_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.biscuit_process_id_seq', 74, true);
          public          postgres    false    205            �           0    0    cake_packaging_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.cake_packaging_id_seq', 1, false);
          public          postgres    false    207            �           0    0    cake_process_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cake_process_id_seq', 1, false);
          public          postgres    false    209            �           0    0    deviation_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.deviation_id_seq', 1, true);
          public          postgres    false    212            �           0    0    ncr_data_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.ncr_data_id_seq', 1, true);
          public          postgres    false    215            �           0    0    plant_parameters_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.plant_parameters_id_seq', 48, true);
          public          postgres    false    220            �           0    0    product_column1_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.product_column1_seq', 17, true);
          public          postgres    false    222            �           0    0    size_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.size_id_seq', 20, true);
          public          postgres    false    225            �           0    0    size_spec_deviations_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.size_spec_deviations_id_seq', 17, true);
          public          postgres    false    227            �           0    0    specification_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.specification_id_seq', 32, true);
          public          postgres    false    229            �           0    0    user_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.user_id_seq', 39, true);
          public          postgres    false    232            �           0    0    user_log_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.user_log_id_seq', 14, true);
          public          postgres    false    234            �           2606    107751 (   biscuit_packaging biscuit_packaging_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_pkey;
       public            postgres    false    202            �           2606    107753 $   biscuit_process biscuit_process_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_pkey;
       public            postgres    false    204            �           2606    107755 "   cake_packaging cake_packaging_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_pkey;
       public            postgres    false    206            �           2606    107757    cake_process cake_process_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_pkey;
       public            postgres    false    208            �           2606    107759    department department_pk 
   CONSTRAINT     X   ALTER TABLE ONLY public.department
    ADD CONSTRAINT department_pk PRIMARY KEY (name);
 B   ALTER TABLE ONLY public.department DROP CONSTRAINT department_pk;
       public            postgres    false    210            �           2606    107761    machine machine_pk 
   CONSTRAINT     b   ALTER TABLE ONLY public.machine
    ADD CONSTRAINT machine_pk PRIMARY KEY (machine_number, name);
 <   ALTER TABLE ONLY public.machine DROP CONSTRAINT machine_pk;
       public            postgres    false    213    213            �           2606    107763    ncr_data ncr_data_pk 
   CONSTRAINT     R   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_pk PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_pk;
       public            postgres    false    214            �           2606    107765     parameter_type parameter_type_pk 
   CONSTRAINT     e   ALTER TABLE ONLY public.parameter_type
    ADD CONSTRAINT parameter_type_pk PRIMARY KEY (type_name);
 J   ALTER TABLE ONLY public.parameter_type DROP CONSTRAINT parameter_type_pk;
       public            postgres    false    217            �           2606    107767 "   plant_parameter plant_parameter_un 
   CONSTRAINT     i   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_parameter_un UNIQUE (plant, parameter);
 L   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_parameter_un;
       public            postgres    false    219    219            �           2606    107769 #   plant_parameter plant_parameters_pk 
   CONSTRAINT     a   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_parameters_pk PRIMARY KEY (id);
 M   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_parameters_pk;
       public            postgres    false    219            �           2606    107771    plant plant_pk 
   CONSTRAINT     T   ALTER TABLE ONLY public.plant
    ADD CONSTRAINT plant_pk PRIMARY KEY (plant_name);
 8   ALTER TABLE ONLY public.plant DROP CONSTRAINT plant_pk;
       public            postgres    false    218            �           2606    107773    product product_pk 
   CONSTRAINT     P   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pk PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.product DROP CONSTRAINT product_pk;
       public            postgres    false    221            �           2606    107775    shift shift_pk 
   CONSTRAINT     N   ALTER TABLE ONLY public.shift
    ADD CONSTRAINT shift_pk PRIMARY KEY (name);
 8   ALTER TABLE ONLY public.shift DROP CONSTRAINT shift_pk;
       public            postgres    false    223            �           2606    107777    size size_pk 
   CONSTRAINT     J   ALTER TABLE ONLY public.size
    ADD CONSTRAINT size_pk PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.size DROP CONSTRAINT size_pk;
       public            postgres    false    224            �           2606    107779 ,   size_spec_deviations size_spec_deviations_pk 
   CONSTRAINT     j   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_pk PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_pk;
       public            postgres    false    226            �           2606    107781 ,   size_spec_deviations size_spec_deviations_un 
   CONSTRAINT     �   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_un UNIQUE (product, plant_parameter, size);
 V   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_un;
       public            postgres    false    226    226    226            �           2606    107783    parameter spec_parameter_pk 
   CONSTRAINT     [   ALTER TABLE ONLY public.parameter
    ADD CONSTRAINT spec_parameter_pk PRIMARY KEY (name);
 E   ALTER TABLE ONLY public.parameter DROP CONSTRAINT spec_parameter_pk;
       public            postgres    false    216            �           2606    107785    parameter spec_parameter_un 
   CONSTRAINT     \   ALTER TABLE ONLY public.parameter
    ADD CONSTRAINT spec_parameter_un UNIQUE (name, type);
 E   ALTER TABLE ONLY public.parameter DROP CONSTRAINT spec_parameter_un;
       public            postgres    false    216    216            �           2606    107787    specification specification_pk 
   CONSTRAINT     \   ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_pk PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_pk;
       public            postgres    false    228            �           2606    107789    specification specification_un 
   CONSTRAINT        ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_un UNIQUE (product, plant_parameter, is_default, size);
 H   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_un;
       public            postgres    false    228    228    228    228            �           2606    107791    unit unit_pk 
   CONSTRAINT     L   ALTER TABLE ONLY public.unit
    ADD CONSTRAINT unit_pk PRIMARY KEY (unit);
 6   ALTER TABLE ONLY public.unit DROP CONSTRAINT unit_pk;
       public            postgres    false    230            �           2606    107793    user_log user_log_pk 
   CONSTRAINT     R   ALTER TABLE ONLY public.user_log
    ADD CONSTRAINT user_log_pk PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.user_log DROP CONSTRAINT user_log_pk;
       public            postgres    false    233            �           2606    107795    user user_pk 
   CONSTRAINT     L   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);
 8   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pk;
       public            postgres    false    231            �           2606    107797    user_role user_role_pk 
   CONSTRAINT     V   ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pk PRIMARY KEY (role);
 @   ALTER TABLE ONLY public.user_role DROP CONSTRAINT user_role_pk;
       public            postgres    false    235            �           2606    107801    user user_unique_email 
   CONSTRAINT     T   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_unique_email UNIQUE (email);
 B   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_unique_email;
       public            postgres    false    231            �           2606    107806 0   biscuit_packaging biscuit_packaging_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 Z   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_product_fkey;
       public          postgres    false    202    221    2972            �           2606    107811 .   biscuit_packaging biscuit_packaging_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 X   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_shift_fkey;
       public          postgres    false    2974    202    223            �           2606    107816 -   biscuit_packaging biscuit_packaging_size_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 W   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_size_fkey;
       public          postgres    false    202    224    2976            �           2606    107821 0   biscuit_packaging biscuit_packaging_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 Z   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_user_id_fkey;
       public          postgres    false    202    231    2988            �           2606    107826 ,   biscuit_process biscuit_process_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 V   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_product_fkey;
       public          postgres    false    204    221    2972            �           2606    107831 *   biscuit_process biscuit_process_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 T   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_shift_fkey;
       public          postgres    false    204    223    2974            �           2606    107836 )   biscuit_process biscuit_process_size_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 S   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_size_fkey;
       public          postgres    false    204    224    2976            �           2606    107841 ,   biscuit_process biscuit_process_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 V   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_user_id_fkey;
       public          postgres    false    231    204    2988            �           2606    107846 *   cake_packaging cake_packaging_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 T   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_product_fkey;
       public          postgres    false    206    221    2972            �           2606    107851 (   cake_packaging cake_packaging_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 R   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_shift_fkey;
       public          postgres    false    206    223    2974            �           2606    107856 '   cake_packaging cake_packaging_size_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 Q   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_size_fkey;
       public          postgres    false    206    224    2976            �           2606    107861 *   cake_packaging cake_packaging_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 T   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_user_id_fkey;
       public          postgres    false    206    231    2988            �           2606    107866 &   cake_process cake_process_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 P   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_product_fkey;
       public          postgres    false    208    221    2972            �           2606    107871 $   cake_process cake_process_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 N   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_shift_fkey;
       public          postgres    false    208    223    2974            �           2606    107876 #   cake_process cake_process_size_fkey    FK CONSTRAINT     ~   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 M   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_size_fkey;
       public          postgres    false    208    224    2976            �           2606    107881 &   cake_process cake_process_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 P   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_user_id_fkey;
       public          postgres    false    208    231    2988            �           2606    107886     deviation deviation_createdby_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_createdby_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 J   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_createdby_fk;
       public          postgres    false    211    231    2988            �           2606    107891    deviation deviation_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_fk FOREIGN KEY (plant_parameter) REFERENCES public.plant_parameter(id) ON UPDATE CASCADE ON DELETE CASCADE;
 @   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_fk;
       public          postgres    false    211    219    2970            �           2606    107896    deviation deviation_plant_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_plant_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_plant_fk;
       public          postgres    false    2966    211    218            �           2606    107901    deviation deviation_product_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_product_fk FOREIGN KEY (product) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE CASCADE;
 H   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_product_fk;
       public          postgres    false    211    2972    221            �           2606    107906    deviation deviation_size_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_size_fk FOREIGN KEY (size) REFERENCES public.size(id) ON UPDATE CASCADE ON DELETE CASCADE;
 E   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_size_fk;
       public          postgres    false    211    2976    224            �           2606    107911    machine machine_fk    FK CONSTRAINT     w   ALTER TABLE ONLY public.machine
    ADD CONSTRAINT machine_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name);
 <   ALTER TABLE ONLY public.machine DROP CONSTRAINT machine_fk;
       public          postgres    false    213    2966    218            �           2606    107916    ncr_data ncr_data_approvedby_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_approvedby_fk FOREIGN KEY (qa_representative) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_approvedby_fk;
       public          postgres    false    231    214    2988            �           2606    107921    ncr_data ncr_data_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_fk FOREIGN KEY (qa_representative) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_fk;
       public          postgres    false    231    2988    214            �           2606    107926    ncr_data ncr_data_plant_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_plant_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_plant_fk;
       public          postgres    false    218    214    2966            �           2606    107931    ncr_data ncr_data_product_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_product_fk FOREIGN KEY (product) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_product_fk;
       public          postgres    false    214    2972    221            �           2606    107936    ncr_data ncr_data_size_fk    FK CONSTRAINT     t   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_size_fk FOREIGN KEY (size) REFERENCES public.size(id);
 C   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_size_fk;
       public          postgres    false    224    2976    214            �           2606    107941    ncr_data ncr_data_user_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_user_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 C   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_user_fk;
       public          postgres    false    214    2988    231            �           2606    107946 #   plant_parameter plant_parameters_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_parameters_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name);
 M   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_parameters_fk;
       public          postgres    false    218    2966    219            �           2606    107951 (   plant_parameter plant_parameters_size_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_parameters_size_fk FOREIGN KEY (unit) REFERENCES public.unit(unit) ON UPDATE CASCADE ON DELETE RESTRICT;
 R   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_parameters_size_fk;
       public          postgres    false    230    2986    219            �           2606    107956    plant_parameter plant_params_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_params_fk FOREIGN KEY (parameter) REFERENCES public.parameter(name);
 I   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_params_fk;
       public          postgres    false    216    2960    219            �           2606    107961    product product_fk    FK CONSTRAINT     w   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name);
 <   ALTER TABLE ONLY public.product DROP CONSTRAINT product_fk;
       public          postgres    false    218    221    2966            �           2606    107966    size size_fk    FK CONSTRAINT     m   ALTER TABLE ONLY public.size
    ADD CONSTRAINT size_fk FOREIGN KEY (product) REFERENCES public.product(id);
 6   ALTER TABLE ONLY public.size DROP CONSTRAINT size_fk;
       public          postgres    false    224    221    2972            �           2606    107971 =   size_spec_deviations size_spec_deviations_fk_plant_parameters    FK CONSTRAINT     �   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_fk_plant_parameters FOREIGN KEY (plant_parameter) REFERENCES public.plant_parameter(id) ON UPDATE CASCADE ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_fk_plant_parameters;
       public          postgres    false    2970    226    219            �           2606    107976 4   size_spec_deviations size_spec_deviations_fk_product    FK CONSTRAINT     �   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_fk_product FOREIGN KEY (product) REFERENCES public.product(id);
 ^   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_fk_product;
       public          postgres    false    2972    226    221            �           2606    107981 1   size_spec_deviations size_spec_deviations_fk_size    FK CONSTRAINT     �   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_fk_size FOREIGN KEY (size) REFERENCES public.size(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_fk_size;
       public          postgres    false    224    2976    226            �           2606    107986    parameter spec_parameter_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.parameter
    ADD CONSTRAINT spec_parameter_fk FOREIGN KEY (parameter_type) REFERENCES public.parameter_type(type_name);
 E   ALTER TABLE ONLY public.parameter DROP CONSTRAINT spec_parameter_fk;
       public          postgres    false    217    2964    216            �           2606    107991    specification specification_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_fk FOREIGN KEY (plant_parameter) REFERENCES public.plant_parameter(id);
 H   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_fk;
       public          postgres    false    2970    219    228            �           2606    107996 &   specification specification_product_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_product_fk FOREIGN KEY (product) REFERENCES public.product(id);
 P   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_product_fk;
       public          postgres    false    221    228    2972            �           2606    108001 #   specification specification_size_fk    FK CONSTRAINT     ~   ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_size_fk FOREIGN KEY (size) REFERENCES public.size(id);
 M   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_size_fk;
       public          postgres    false    224    228    2976            �           2606    108006    user user_department_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_department_fk FOREIGN KEY (department) REFERENCES public.department(name) ON UPDATE CASCADE ON DELETE RESTRICT;
 C   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_department_fk;
       public          postgres    false    2954    210    231            �           2606    108011    user user_fk    FK CONSTRAINT     p   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_fk FOREIGN KEY (role) REFERENCES public.user_role(role);
 8   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_fk;
       public          postgres    false    235    2994    231            �           2606    108016    user_log user_log_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_log
    ADD CONSTRAINT user_log_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.user_log DROP CONSTRAINT user_log_fk;
       public          postgres    false    231    233    2988            �           2606    108021    user_log user_log_fk_shift    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_log
    ADD CONSTRAINT user_log_fk_shift FOREIGN KEY (shift) REFERENCES public.shift(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
 D   ALTER TABLE ONLY public.user_log DROP CONSTRAINT user_log_fk_shift;
       public          postgres    false    223    233    2974            �           2606    108026    user_log user_log_plant_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_log
    ADD CONSTRAINT user_log_plant_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name) ON UPDATE RESTRICT ON DELETE RESTRICT;
 D   ALTER TABLE ONLY public.user_log DROP CONSTRAINT user_log_plant_fk;
       public          postgres    false    218    233    2966            ^      x������ � �      `   F  x���[n�0�o�
6 ��	,�+�Qp	��d�%��I#���#ϝ1�@@�-ʝ(�ں֍�9��B@h@���(!9�"�6o���X��/'O~=�yx�*����W˜>�(�>�^8gl�q�m�Um��M��q�Wi�qY�{�{S�i�O ��� �l�љ+�f��.�N�C���`Z�*�Y|U�f?�������X@�<����٦x��ƚ1ڊ\��-�:J��;ӝ�QK=Y��5�����_�\׾�\ �s����袗��w��t+�'�������?
$��M2�[�[^����T7�r��`�xڛ�6��y��^      b      x������ � �      d      x������ � �      f   I   x�u����,M��,�Tp,..-J�KNUpI-H,*�M�+�
�w	u����(�O)M.���CV���� ڈ<      g   B   x�3�445�44�12�44�4202�50�52T04�22�26�41�47�t�,N.�,*������ H�7      i      x������ � �      j   D   x�3�44��C ##C]]#K+S+#N������NC#N a����Rnn����� 2�a      l   �   x�+�����K-.���ur��wv�,�4202�50�52S02�20�25��H�L�(!Nm~J~i�������\mV�iE�y%�e��圞����(�LN��"�5��9�3����������D��H�������Ȅ+7?����(�8���qqq K�F
      m   �   x�M��
�0E��+�t�.� �� ��X�I IK�ϷUK{7�p�[��܄�y
�i��B��y)V@[
��,+����L�O�k�b����ۢX�M�v�ۚ�v�N���O�E��.�'yx+�(P8U4`��cT#�
�H<:I0��z�b�+?-�h����{lc��R�      n   4   x�sN�N�,���4202�50�52S02�20�21�r�,N.�,�� F��� #��      o   �   x����
!@���)f���;:0����3�A��3�L��8�^��&]l�p%B� rʐ��a��Tl$�5�M�DS�j��f���ٛ�*v?�߇[>��<+�o�e?4�V�7��#��qTrTr B��2��ç5!���Cl      q   �   x���1n� �����1Jb��Щ=A�&q ��&�O[U��.���e]�g���9���� a���1��&�;��b�W���p��lj�]�bi�[P�V&��=4�`��LO���;{X͠}�;ǅ@l��d|�FΚ�3�̋J�~R�?.��p�?�5e[�z�x[��s��2��%�t���+������욦y �g%      s   "   x�+��L+�5��"�b0��g���qqq I�p      t   �   x�}λ
�@������Pv���
�"�ڬ�F11�&E����
�14mc�U!�d��@���
Z\�<ԗ��C<��Mj�&ԑ`ߙ����M�5�':}"�c�)�PU�
��=���W�-�9FA��>4��lNa,�3&4�ˑ�^��#�^<�hZ�T�_b��!3? 8�@      v   o   x�}���0��4�p J��h����Q+�E�y 	��	N/�����V���!�PjB@����������b���q�I Y!�n��=�y�7��BV�����o�N)�      x   �   x�m���0�og
��g;�z�N�o;@E��iA�R����\����{Ӄ ���gv䴬RW��,'�pp�\e�!�o����A�W����ĩ}��f�Ԉz��V!�t���"�����f�S}�����`��/�n,>�S��$�d��-Y_�?�ur�4_RJo.�D      z   }   x�5�1�0Eg�YX��s#�̒�&�;�� q{��Oz�Y �v�d��s�c�)Yo��C��=��/^G�a(wG���@�$�[��k�A�\A[i�+�T��Ⱦ�xw�Ì4��qF��01z      {   &  x�͔Ko�0��ԯ�!WK$e=OUl�QQ��30�-˖H��\���J�-��E ���7ۯ3�����<���~6���?�Fh��F�{?Ϭ���%�K�/�_2�i�R� w ����x�p#���$_�5��	�%Kn�t���˚!b{"�8O��5�%�\�*������m��P�aH��u&�8� }-r�I��5�f�E& 6�&���I��b�flUlE%�26��F��=�Lƾo�]� �)�`ڡ�5}���j�m����A���&�*�{�y�O�N�o��k7�k�<n����W6��ts���N���C(��f ���1F�A%���bja�
�9�N�����s��v,��@�c�����M�ͶPm[���鸿�ͣ�h5F��pz&��*����1�Fi̀�����<D�s�'��C�w|L�@�\Ā�-��s���c�I9����ӥ�c����YEw>�%�cW�� A�2��_��<������;a�=�<��&�e�N*�&��c˷���?�y�5M�	&���      }   b   x��K@@ �u���ȴ$��X��T4�)���[=ʠ�8\j@�YN��G]�⬓y�z_M�C�ή�rkg�o80��z.����k�����         �   x�m��
�0��cw^A�0t��Q�˜1�	ۻȻψD���ǟGE�[\RB��I�8Zm|�h/��g�&�����ü;��k�����>5AKz\k������n[�Z�]�rYn�Y�)��d��g�4��e�����!��K���!�i1M�     