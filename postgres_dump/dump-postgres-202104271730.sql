PGDMP                         y            postgres    12.3 (Debian 12.3-1.pgdg100+1) #   12.6 (Ubuntu 12.6-0ubuntu0.20.04.1) �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    13408    postgres    DATABASE     x   CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
    DROP DATABASE postgres;
                postgres    false            �           0    0    DATABASE postgres    COMMENT     N   COMMENT ON DATABASE postgres IS 'default administrative connection database';
                   postgres    false    3243                        2615    75121    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
                postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                   postgres    false    5            �            1259    75122    biscuit_packaging    TABLE     �  CREATE TABLE public.biscuit_packaging (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL,
    is_deviated boolean DEFAULT false,
    "Price" double precision,
    manufacture_date timestamp without time zone,
    line_leader character varying,
    price double precision
);
 %   DROP TABLE public.biscuit_packaging;
       public         heap    postgres    false    5            �            1259    75130    biscuit_packaging_id_seq    SEQUENCE     �   CREATE SEQUENCE public.biscuit_packaging_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.biscuit_packaging_id_seq;
       public          postgres    false    202    5            �           0    0    biscuit_packaging_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.biscuit_packaging_id_seq OWNED BY public.biscuit_packaging.id;
          public          postgres    false    203            �            1259    75132    biscuit_process    TABLE     �  CREATE TABLE public.biscuit_process (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL,
    odour boolean,
    moisture double precision,
    thinkness double precision,
    front_view character varying,
    is_deviated boolean DEFAULT false
);
 #   DROP TABLE public.biscuit_process;
       public         heap    postgres    false    5            �            1259    75140    biscuit_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.biscuit_process_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.biscuit_process_id_seq;
       public          postgres    false    204    5            �           0    0    biscuit_process_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.biscuit_process_id_seq OWNED BY public.biscuit_process.id;
          public          postgres    false    205            �            1259    75142    cake_packaging    TABLE     @  CREATE TABLE public.cake_packaging (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL,
    is_deviated boolean DEFAULT false
);
 "   DROP TABLE public.cake_packaging;
       public         heap    postgres    false    5            �            1259    75150    cake_packaging_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cake_packaging_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.cake_packaging_id_seq;
       public          postgres    false    5    206            �           0    0    cake_packaging_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.cake_packaging_id_seq OWNED BY public.cake_packaging.id;
          public          postgres    false    207            �            1259    75152    cake_process    TABLE     ]  CREATE TABLE public.cake_process (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL,
    moisture double precision,
    is_deviated boolean DEFAULT false
);
     DROP TABLE public.cake_process;
       public         heap    postgres    false    5            �            1259    75160    cake_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.cake_process_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.cake_process_id_seq;
       public          postgres    false    5    208            �           0    0    cake_process_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.cake_process_id_seq OWNED BY public.cake_process.id;
          public          postgres    false    209            �            1259    75162 
   department    TABLE     k   CREATE TABLE public.department (
    name character varying NOT NULL,
    description character varying
);
    DROP TABLE public.department;
       public         heap    postgres    false    5            �            1259    75168 	   deviation    TABLE     �  CREATE TABLE public.deviation (
    id integer NOT NULL,
    value character varying NOT NULL,
    specification character varying NOT NULL,
    created_by integer NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    plant_parameter integer NOT NULL,
    record_id integer NOT NULL,
    plant character varying NOT NULL,
    product integer NOT NULL,
    size integer NOT NULL
);
    DROP TABLE public.deviation;
       public         heap    postgres    false    5            �            1259    75175    deviation_id_seq    SEQUENCE     �   CREATE SEQUENCE public.deviation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.deviation_id_seq;
       public          postgres    false    5    211            �           0    0    deviation_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.deviation_id_seq OWNED BY public.deviation.id;
          public          postgres    false    212            �            1259    75177    machine    TABLE     �   CREATE TABLE public.machine (
    name character varying NOT NULL,
    machine_number character varying NOT NULL,
    is_active character varying DEFAULT true NOT NULL,
    plant character varying
);
    DROP TABLE public.machine;
       public         heap    postgres    false    5            �            1259    75184    ncr_data    TABLE     �  CREATE TABLE public.ncr_data (
    id integer NOT NULL,
    created_by integer,
    qa_representative integer,
    deviated_parameters character varying,
    reason character varying,
    production_representative integer,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    plant character varying NOT NULL,
    product integer NOT NULL,
    is_active boolean DEFAULT true,
    size integer NOT NULL,
    type character varying NOT NULL,
    comment character varying,
    record_id integer NOT NULL,
    CONSTRAINT ncr_data_check CHECK (((type)::text = ANY (ARRAY[('PACKAGING'::character varying)::text, ('PROCESS'::character varying)::text])))
);
    DROP TABLE public.ncr_data;
       public         heap    postgres    false    5            �           0    0    COLUMN ncr_data.reason    COMMENT     R   COMMENT ON COLUMN public.ncr_data.reason IS 'to be filled by production manager';
          public          postgres    false    214            �           0    0 )   COLUMN ncr_data.production_representative    COMMENT     n   COMMENT ON COLUMN public.ncr_data.production_representative IS 'to be filled by relavent production manager';
          public          postgres    false    214            �            1259    75191    ncr_data_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ncr_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.ncr_data_id_seq;
       public          postgres    false    5    214            �           0    0    ncr_data_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.ncr_data_id_seq OWNED BY public.ncr_data.id;
          public          postgres    false    215            �            1259    75193 	   parameter    TABLE     �  CREATE TABLE public.parameter (
    name character varying NOT NULL,
    parameter_type character varying NOT NULL,
    type character varying NOT NULL,
    is_active boolean DEFAULT true,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    CONSTRAINT spec_parameter_type_check CHECK (((type)::text = ANY (ARRAY[('PACKAGING'::character varying)::text, ('PROCESS'::character varying)::text])))
);
    DROP TABLE public.parameter;
       public         heap    postgres    false    5            �            1259    75202    parameter_type    TABLE     t   CREATE TABLE public.parameter_type (
    type_name character varying NOT NULL,
    description character varying
);
 "   DROP TABLE public.parameter_type;
       public         heap    postgres    false    5            �           0    0    TABLE parameter_type    COMMENT     8   COMMENT ON TABLE public.parameter_type IS 'Enum Table';
          public          postgres    false    217            �            1259    75208    plant    TABLE     �   CREATE TABLE public.plant (
    plant_name character varying NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    image_url character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.plant;
       public         heap    postgres    false    5            �            1259    75216    plant_parameter    TABLE        CREATE TABLE public.plant_parameter (
    id integer NOT NULL,
    plant character varying NOT NULL,
    parameter character varying NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    unit character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
 #   DROP TABLE public.plant_parameter;
       public         heap    postgres    false    5            �            1259    75224    plant_parameters_id_seq    SEQUENCE     �   CREATE SEQUENCE public.plant_parameters_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.plant_parameters_id_seq;
       public          postgres    false    5    219            �           0    0    plant_parameters_id_seq    SEQUENCE OWNED BY     R   ALTER SEQUENCE public.plant_parameters_id_seq OWNED BY public.plant_parameter.id;
          public          postgres    false    220            �            1259    75226    product    TABLE       CREATE TABLE public.product (
    plant character varying NOT NULL,
    name character varying NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    id integer NOT NULL,
    image_url character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.product;
       public         heap    postgres    false    5            �            1259    75234    product_column1_seq    SEQUENCE     �   CREATE SEQUENCE public.product_column1_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.product_column1_seq;
       public          postgres    false    221    5            �           0    0    product_column1_seq    SEQUENCE OWNED BY     F   ALTER SEQUENCE public.product_column1_seq OWNED BY public.product.id;
          public          postgres    false    222            �            1259    75236    shift    TABLE     �   CREATE TABLE public.shift (
    name character varying NOT NULL,
    start_time time without time zone,
    end_time time without time zone
);
    DROP TABLE public.shift;
       public         heap    postgres    false    5            �            1259    75242    size    TABLE     �   CREATE TABLE public.size (
    id integer NOT NULL,
    size_name character varying NOT NULL,
    product integer NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now()
);
    DROP TABLE public.size;
       public         heap    postgres    false    5            �           0    0    COLUMN size.size_name    COMMENT     J   COMMENT ON COLUMN public.size.size_name IS '5g, 10g or medium large etc';
          public          postgres    false    224            �            1259    75250    size_id_seq    SEQUENCE     �   CREATE SEQUENCE public.size_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.size_id_seq;
       public          postgres    false    224    5            �           0    0    size_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.size_id_seq OWNED BY public.size.id;
          public          postgres    false    225            �            1259    75252    size_spec_deviations    TABLE       CREATE TABLE public.size_spec_deviations (
    product integer NOT NULL,
    plant_parameter integer NOT NULL,
    size integer NOT NULL,
    is_active boolean DEFAULT false NOT NULL,
    id integer NOT NULL,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
 (   DROP TABLE public.size_spec_deviations;
       public         heap    postgres    false    5            �            1259    75257    size_spec_deviations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.size_spec_deviations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.size_spec_deviations_id_seq;
       public          postgres    false    5    226            �           0    0    size_spec_deviations_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.size_spec_deviations_id_seq OWNED BY public.size_spec_deviations.id;
          public          postgres    false    227            �            1259    75259    specification    TABLE     J  CREATE TABLE public.specification (
    is_active boolean DEFAULT true NOT NULL,
    product integer NOT NULL,
    id integer NOT NULL,
    value character varying,
    plant_parameter integer NOT NULL,
    is_default boolean DEFAULT true,
    size integer,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
 !   DROP TABLE public.specification;
       public         heap    postgres    false    5            �            1259    75268    specification_id_seq    SEQUENCE     �   CREATE SEQUENCE public.specification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.specification_id_seq;
       public          postgres    false    5    228            �           0    0    specification_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.specification_id_seq OWNED BY public.specification.id;
          public          postgres    false    229            �            1259    75270    unit    TABLE     e   CREATE TABLE public.unit (
    unit character varying NOT NULL,
    description character varying
);
    DROP TABLE public.unit;
       public         heap    postgres    false    5            �            1259    75276    user    TABLE     )  CREATE TABLE public."user" (
    first_name character varying,
    last_name character varying,
    role character varying NOT NULL,
    department character varying,
    user_name character varying,
    password character varying NOT NULL,
    emp_no character varying NOT NULL,
    id integer NOT NULL,
    email character varying,
    is_active boolean DEFAULT true,
    otp character varying,
    otp_expiry timestamp(0) without time zone,
    profile_photo character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public."user";
       public         heap    postgres    false    5            �            1259    75284    user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.user_id_seq;
       public          postgres    false    5    231            �           0    0    user_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;
          public          postgres    false    232            �            1259    75286    user_log    TABLE       CREATE TABLE public.user_log (
    id integer NOT NULL,
    plant character varying NOT NULL,
    created_by integer NOT NULL,
    log text,
    shift text NOT NULL,
    title character varying,
    created_at timestamp(0) without time zone DEFAULT now() NOT NULL
);
    DROP TABLE public.user_log;
       public         heap    postgres    false    5            �            1259    75293    user_log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.user_log_id_seq;
       public          postgres    false    233    5            �           0    0    user_log_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.user_log_id_seq OWNED BY public.user_log.id;
          public          postgres    false    234            �            1259    75295 	   user_role    TABLE     s   CREATE TABLE public.user_role (
    description character varying(256),
    role character varying(32) NOT NULL
);
    DROP TABLE public.user_role;
       public         heap    postgres    false    5            �            1259    99264    wafer_sticks_packaging    TABLE     H  CREATE TABLE public.wafer_sticks_packaging (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    is_deviated boolean DEFAULT false,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL
);
 *   DROP TABLE public.wafer_sticks_packaging;
       public         heap    postgres    false    5            �            1259    99262    wafer_sticks_packaging_id_seq    SEQUENCE     �   CREATE SEQUENCE public.wafer_sticks_packaging_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.wafer_sticks_packaging_id_seq;
       public          postgres    false    5    237            �           0    0    wafer_sticks_packaging_id_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public.wafer_sticks_packaging_id_seq OWNED BY public.wafer_sticks_packaging.id;
          public          postgres    false    236            �            1259    99297    wafer_sticks_process    TABLE     F  CREATE TABLE public.wafer_sticks_process (
    id integer NOT NULL,
    user_id integer NOT NULL,
    size integer NOT NULL,
    product integer NOT NULL,
    shift character varying NOT NULL,
    is_deviated boolean DEFAULT false,
    comment character varying,
    date timestamp without time zone DEFAULT now() NOT NULL
);
 (   DROP TABLE public.wafer_sticks_process;
       public         heap    postgres    false    5            �            1259    99295    wafer_sticks_process_id_seq    SEQUENCE     �   CREATE SEQUENCE public.wafer_sticks_process_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.wafer_sticks_process_id_seq;
       public          postgres    false    5    239            �           0    0    wafer_sticks_process_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.wafer_sticks_process_id_seq OWNED BY public.wafer_sticks_process.id;
          public          postgres    false    238            d           2604    75298    biscuit_packaging id    DEFAULT     |   ALTER TABLE ONLY public.biscuit_packaging ALTER COLUMN id SET DEFAULT nextval('public.biscuit_packaging_id_seq'::regclass);
 C   ALTER TABLE public.biscuit_packaging ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202            g           2604    75299    biscuit_process id    DEFAULT     x   ALTER TABLE ONLY public.biscuit_process ALTER COLUMN id SET DEFAULT nextval('public.biscuit_process_id_seq'::regclass);
 A   ALTER TABLE public.biscuit_process ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    205    204            j           2604    75300    cake_packaging id    DEFAULT     v   ALTER TABLE ONLY public.cake_packaging ALTER COLUMN id SET DEFAULT nextval('public.cake_packaging_id_seq'::regclass);
 @   ALTER TABLE public.cake_packaging ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206            m           2604    75301    cake_process id    DEFAULT     r   ALTER TABLE ONLY public.cake_process ALTER COLUMN id SET DEFAULT nextval('public.cake_process_id_seq'::regclass);
 >   ALTER TABLE public.cake_process ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    209    208            o           2604    75302    deviation id    DEFAULT     l   ALTER TABLE ONLY public.deviation ALTER COLUMN id SET DEFAULT nextval('public.deviation_id_seq'::regclass);
 ;   ALTER TABLE public.deviation ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    211            s           2604    75303    ncr_data id    DEFAULT     j   ALTER TABLE ONLY public.ncr_data ALTER COLUMN id SET DEFAULT nextval('public.ncr_data_id_seq'::regclass);
 :   ALTER TABLE public.ncr_data ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    215    214            |           2604    75304    plant_parameter id    DEFAULT     y   ALTER TABLE ONLY public.plant_parameter ALTER COLUMN id SET DEFAULT nextval('public.plant_parameters_id_seq'::regclass);
 A   ALTER TABLE public.plant_parameter ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    219                       2604    75305 
   product id    DEFAULT     m   ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_column1_seq'::regclass);
 9   ALTER TABLE public.product ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    221            �           2604    75306    size id    DEFAULT     b   ALTER TABLE ONLY public.size ALTER COLUMN id SET DEFAULT nextval('public.size_id_seq'::regclass);
 6   ALTER TABLE public.size ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    225    224            �           2604    75307    size_spec_deviations id    DEFAULT     �   ALTER TABLE ONLY public.size_spec_deviations ALTER COLUMN id SET DEFAULT nextval('public.size_spec_deviations_id_seq'::regclass);
 F   ALTER TABLE public.size_spec_deviations ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226            �           2604    75308    specification id    DEFAULT     t   ALTER TABLE ONLY public.specification ALTER COLUMN id SET DEFAULT nextval('public.specification_id_seq'::regclass);
 ?   ALTER TABLE public.specification ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    229    228            �           2604    75309    user id    DEFAULT     d   ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);
 8   ALTER TABLE public."user" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    232    231            �           2604    75310    user_log id    DEFAULT     j   ALTER TABLE ONLY public.user_log ALTER COLUMN id SET DEFAULT nextval('public.user_log_id_seq'::regclass);
 :   ALTER TABLE public.user_log ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    234    233            �           2604    99267    wafer_sticks_packaging id    DEFAULT     �   ALTER TABLE ONLY public.wafer_sticks_packaging ALTER COLUMN id SET DEFAULT nextval('public.wafer_sticks_packaging_id_seq'::regclass);
 H   ALTER TABLE public.wafer_sticks_packaging ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    236    237    237            �           2604    99300    wafer_sticks_process id    DEFAULT     �   ALTER TABLE ONLY public.wafer_sticks_process ALTER COLUMN id SET DEFAULT nextval('public.wafer_sticks_process_id_seq'::regclass);
 F   ALTER TABLE public.wafer_sticks_process ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    238    239    239            �          0    75122    biscuit_packaging 
   TABLE DATA           �   COPY public.biscuit_packaging (id, user_id, size, product, shift, comment, date, is_deviated, "Price", manufacture_date, line_leader, price) FROM stdin;
    public          postgres    false    202            �          0    75132    biscuit_process 
   TABLE DATA           �   COPY public.biscuit_process (id, user_id, size, product, shift, comment, date, odour, moisture, thinkness, front_view, is_deviated) FROM stdin;
    public          postgres    false    204            �          0    75142    cake_packaging 
   TABLE DATA           g   COPY public.cake_packaging (id, user_id, size, product, shift, comment, date, is_deviated) FROM stdin;
    public          postgres    false    206            �          0    75152    cake_process 
   TABLE DATA           o   COPY public.cake_process (id, user_id, size, product, shift, comment, date, moisture, is_deviated) FROM stdin;
    public          postgres    false    208            �          0    75162 
   department 
   TABLE DATA           7   COPY public.department (name, description) FROM stdin;
    public          postgres    false    210            �          0    75168 	   deviation 
   TABLE DATA           �   COPY public.deviation (id, value, specification, created_by, created_at, plant_parameter, record_id, plant, product, size) FROM stdin;
    public          postgres    false    211            �          0    75177    machine 
   TABLE DATA           I   COPY public.machine (name, machine_number, is_active, plant) FROM stdin;
    public          postgres    false    213            �          0    75184    ncr_data 
   TABLE DATA           �   COPY public.ncr_data (id, created_by, qa_representative, deviated_parameters, reason, production_representative, created_at, plant, product, is_active, size, type, comment, record_id) FROM stdin;
    public          postgres    false    214            �          0    75193 	   parameter 
   TABLE DATA           V   COPY public.parameter (name, parameter_type, type, is_active, created_at) FROM stdin;
    public          postgres    false    216            �          0    75202    parameter_type 
   TABLE DATA           @   COPY public.parameter_type (type_name, description) FROM stdin;
    public          postgres    false    217            �          0    75208    plant 
   TABLE DATA           M   COPY public.plant (plant_name, is_active, image_url, created_at) FROM stdin;
    public          postgres    false    218            �          0    75216    plant_parameter 
   TABLE DATA           \   COPY public.plant_parameter (id, plant, parameter, is_active, unit, created_at) FROM stdin;
    public          postgres    false    219            �          0    75226    product 
   TABLE DATA           T   COPY public.product (plant, name, is_active, id, image_url, created_at) FROM stdin;
    public          postgres    false    221            �          0    75236    shift 
   TABLE DATA           ;   COPY public.shift (name, start_time, end_time) FROM stdin;
    public          postgres    false    223            �          0    75242    size 
   TABLE DATA           M   COPY public.size (id, size_name, product, is_active, created_at) FROM stdin;
    public          postgres    false    224            �          0    75252    size_spec_deviations 
   TABLE DATA           i   COPY public.size_spec_deviations (product, plant_parameter, size, is_active, id, created_at) FROM stdin;
    public          postgres    false    226            �          0    75259    specification 
   TABLE DATA           u   COPY public.specification (is_active, product, id, value, plant_parameter, is_default, size, created_at) FROM stdin;
    public          postgres    false    228            �          0    75270    unit 
   TABLE DATA           1   COPY public.unit (unit, description) FROM stdin;
    public          postgres    false    230            �          0    75276    user 
   TABLE DATA           �   COPY public."user" (first_name, last_name, role, department, user_name, password, emp_no, id, email, is_active, otp, otp_expiry, profile_photo, created_at) FROM stdin;
    public          postgres    false    231            �          0    75286    user_log 
   TABLE DATA           X   COPY public.user_log (id, plant, created_by, log, shift, title, created_at) FROM stdin;
    public          postgres    false    233            �          0    75295 	   user_role 
   TABLE DATA           6   COPY public.user_role (description, role) FROM stdin;
    public          postgres    false    235            �          0    99264    wafer_sticks_packaging 
   TABLE DATA           o   COPY public.wafer_sticks_packaging (id, user_id, size, product, shift, is_deviated, comment, date) FROM stdin;
    public          postgres    false    237            �          0    99297    wafer_sticks_process 
   TABLE DATA           m   COPY public.wafer_sticks_process (id, user_id, size, product, shift, is_deviated, comment, date) FROM stdin;
    public          postgres    false    239            �           0    0    biscuit_packaging_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.biscuit_packaging_id_seq', 5, true);
          public          postgres    false    203            �           0    0    biscuit_process_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.biscuit_process_id_seq', 74, true);
          public          postgres    false    205            �           0    0    cake_packaging_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.cake_packaging_id_seq', 1, false);
          public          postgres    false    207            �           0    0    cake_process_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.cake_process_id_seq', 1, false);
          public          postgres    false    209            �           0    0    deviation_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.deviation_id_seq', 2, true);
          public          postgres    false    212            �           0    0    ncr_data_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.ncr_data_id_seq', 2, true);
          public          postgres    false    215            �           0    0    plant_parameters_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.plant_parameters_id_seq', 58, true);
          public          postgres    false    220            �           0    0    product_column1_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.product_column1_seq', 14, true);
          public          postgres    false    222            �           0    0    size_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.size_id_seq', 20, true);
          public          postgres    false    225            �           0    0    size_spec_deviations_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.size_spec_deviations_id_seq', 18, true);
          public          postgres    false    227            �           0    0    specification_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.specification_id_seq', 33, true);
          public          postgres    false    229            �           0    0    user_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.user_id_seq', 25, true);
          public          postgres    false    232            �           0    0    user_log_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.user_log_id_seq', 14, true);
          public          postgres    false    234            �           0    0    wafer_sticks_packaging_id_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public.wafer_sticks_packaging_id_seq', 1, false);
          public          postgres    false    236            �           0    0    wafer_sticks_process_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.wafer_sticks_process_id_seq', 1, false);
          public          postgres    false    238            �           2606    75312 (   biscuit_packaging biscuit_packaging_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_pkey;
       public            postgres    false    202            �           2606    75314 $   biscuit_process biscuit_process_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_pkey;
       public            postgres    false    204            �           2606    75316 "   cake_packaging cake_packaging_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_pkey;
       public            postgres    false    206            �           2606    75318    cake_process cake_process_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_pkey;
       public            postgres    false    208            �           2606    75320    department department_pk 
   CONSTRAINT     X   ALTER TABLE ONLY public.department
    ADD CONSTRAINT department_pk PRIMARY KEY (name);
 B   ALTER TABLE ONLY public.department DROP CONSTRAINT department_pk;
       public            postgres    false    210            �           2606    75322    machine machine_pk 
   CONSTRAINT     b   ALTER TABLE ONLY public.machine
    ADD CONSTRAINT machine_pk PRIMARY KEY (machine_number, name);
 <   ALTER TABLE ONLY public.machine DROP CONSTRAINT machine_pk;
       public            postgres    false    213    213            �           2606    75324    ncr_data ncr_data_pk 
   CONSTRAINT     R   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_pk PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_pk;
       public            postgres    false    214            �           2606    75326     parameter_type parameter_type_pk 
   CONSTRAINT     e   ALTER TABLE ONLY public.parameter_type
    ADD CONSTRAINT parameter_type_pk PRIMARY KEY (type_name);
 J   ALTER TABLE ONLY public.parameter_type DROP CONSTRAINT parameter_type_pk;
       public            postgres    false    217            �           2606    75328 "   plant_parameter plant_parameter_un 
   CONSTRAINT     i   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_parameter_un UNIQUE (plant, parameter);
 L   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_parameter_un;
       public            postgres    false    219    219            �           2606    75330 #   plant_parameter plant_parameters_pk 
   CONSTRAINT     a   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_parameters_pk PRIMARY KEY (id);
 M   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_parameters_pk;
       public            postgres    false    219            �           2606    75332    plant plant_pk 
   CONSTRAINT     T   ALTER TABLE ONLY public.plant
    ADD CONSTRAINT plant_pk PRIMARY KEY (plant_name);
 8   ALTER TABLE ONLY public.plant DROP CONSTRAINT plant_pk;
       public            postgres    false    218            �           2606    75334    product product_pk 
   CONSTRAINT     P   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pk PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.product DROP CONSTRAINT product_pk;
       public            postgres    false    221            �           2606    75336    shift shift_pk 
   CONSTRAINT     N   ALTER TABLE ONLY public.shift
    ADD CONSTRAINT shift_pk PRIMARY KEY (name);
 8   ALTER TABLE ONLY public.shift DROP CONSTRAINT shift_pk;
       public            postgres    false    223            �           2606    75338    size size_pk 
   CONSTRAINT     J   ALTER TABLE ONLY public.size
    ADD CONSTRAINT size_pk PRIMARY KEY (id);
 6   ALTER TABLE ONLY public.size DROP CONSTRAINT size_pk;
       public            postgres    false    224            �           2606    75340 ,   size_spec_deviations size_spec_deviations_pk 
   CONSTRAINT     j   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_pk PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_pk;
       public            postgres    false    226            �           2606    75342 ,   size_spec_deviations size_spec_deviations_un 
   CONSTRAINT     �   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_un UNIQUE (product, plant_parameter, size);
 V   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_un;
       public            postgres    false    226    226    226            �           2606    75344    parameter spec_parameter_pk 
   CONSTRAINT     [   ALTER TABLE ONLY public.parameter
    ADD CONSTRAINT spec_parameter_pk PRIMARY KEY (name);
 E   ALTER TABLE ONLY public.parameter DROP CONSTRAINT spec_parameter_pk;
       public            postgres    false    216            �           2606    75346    parameter spec_parameter_un 
   CONSTRAINT     \   ALTER TABLE ONLY public.parameter
    ADD CONSTRAINT spec_parameter_un UNIQUE (name, type);
 E   ALTER TABLE ONLY public.parameter DROP CONSTRAINT spec_parameter_un;
       public            postgres    false    216    216            �           2606    75348    specification specification_pk 
   CONSTRAINT     \   ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_pk PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_pk;
       public            postgres    false    228            �           2606    75350    specification specification_un 
   CONSTRAINT        ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_un UNIQUE (product, plant_parameter, is_default, size);
 H   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_un;
       public            postgres    false    228    228    228    228            �           2606    75352    unit unit_pk 
   CONSTRAINT     L   ALTER TABLE ONLY public.unit
    ADD CONSTRAINT unit_pk PRIMARY KEY (unit);
 6   ALTER TABLE ONLY public.unit DROP CONSTRAINT unit_pk;
       public            postgres    false    230            �           2606    75354    user_log user_log_pk 
   CONSTRAINT     R   ALTER TABLE ONLY public.user_log
    ADD CONSTRAINT user_log_pk PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.user_log DROP CONSTRAINT user_log_pk;
       public            postgres    false    233            �           2606    75356    user user_pk 
   CONSTRAINT     L   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);
 8   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_pk;
       public            postgres    false    231            �           2606    75358    user_role user_role_pk 
   CONSTRAINT     V   ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pk PRIMARY KEY (role);
 @   ALTER TABLE ONLY public.user_role DROP CONSTRAINT user_role_pk;
       public            postgres    false    235            �           2606    75360    user user_un 
   CONSTRAINT     K   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_un UNIQUE (emp_no);
 8   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_un;
       public            postgres    false    231            �           2606    75362    user user_unique_email 
   CONSTRAINT     T   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_unique_email UNIQUE (email);
 B   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_unique_email;
       public            postgres    false    231            �           2606    99274 2   wafer_sticks_packaging wafer_sticks_packaging_pkey 
   CONSTRAINT     p   ALTER TABLE ONLY public.wafer_sticks_packaging
    ADD CONSTRAINT wafer_sticks_packaging_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.wafer_sticks_packaging DROP CONSTRAINT wafer_sticks_packaging_pkey;
       public            postgres    false    237            �           2606    99307 .   wafer_sticks_process wafer_sticks_process_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.wafer_sticks_process
    ADD CONSTRAINT wafer_sticks_process_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.wafer_sticks_process DROP CONSTRAINT wafer_sticks_process_pkey;
       public            postgres    false    239            �           2606    75363 0   biscuit_packaging biscuit_packaging_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 Z   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_product_fkey;
       public          postgres    false    2992    202    221            �           2606    75368 .   biscuit_packaging biscuit_packaging_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 X   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_shift_fkey;
       public          postgres    false    223    202    2994            �           2606    75373 -   biscuit_packaging biscuit_packaging_size_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 W   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_size_fkey;
       public          postgres    false    224    2996    202            �           2606    75378 0   biscuit_packaging biscuit_packaging_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_packaging
    ADD CONSTRAINT biscuit_packaging_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 Z   ALTER TABLE ONLY public.biscuit_packaging DROP CONSTRAINT biscuit_packaging_user_id_fkey;
       public          postgres    false    202    231    3008            �           2606    75383 ,   biscuit_process biscuit_process_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 V   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_product_fkey;
       public          postgres    false    204    2992    221            �           2606    75388 *   biscuit_process biscuit_process_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 T   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_shift_fkey;
       public          postgres    false    223    204    2994            �           2606    75393 )   biscuit_process biscuit_process_size_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 S   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_size_fkey;
       public          postgres    false    2996    204    224            �           2606    75398 ,   biscuit_process biscuit_process_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.biscuit_process
    ADD CONSTRAINT biscuit_process_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 V   ALTER TABLE ONLY public.biscuit_process DROP CONSTRAINT biscuit_process_user_id_fkey;
       public          postgres    false    3008    204    231            �           2606    75403 *   cake_packaging cake_packaging_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 T   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_product_fkey;
       public          postgres    false    221    2992    206            �           2606    75408 (   cake_packaging cake_packaging_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 R   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_shift_fkey;
       public          postgres    false    206    223    2994            �           2606    75413 '   cake_packaging cake_packaging_size_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 Q   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_size_fkey;
       public          postgres    false    2996    224    206            �           2606    75418 *   cake_packaging cake_packaging_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_packaging
    ADD CONSTRAINT cake_packaging_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 T   ALTER TABLE ONLY public.cake_packaging DROP CONSTRAINT cake_packaging_user_id_fkey;
       public          postgres    false    206    231    3008            �           2606    75423 &   cake_process cake_process_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 P   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_product_fkey;
       public          postgres    false    2992    208    221            �           2606    75428 $   cake_process cake_process_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 N   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_shift_fkey;
       public          postgres    false    2994    223    208            �           2606    75433 #   cake_process cake_process_size_fkey    FK CONSTRAINT     ~   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 M   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_size_fkey;
       public          postgres    false    208    2996    224            �           2606    75438 &   cake_process cake_process_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.cake_process
    ADD CONSTRAINT cake_process_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 P   ALTER TABLE ONLY public.cake_process DROP CONSTRAINT cake_process_user_id_fkey;
       public          postgres    false    231    208    3008            �           2606    75443     deviation deviation_createdby_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_createdby_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 J   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_createdby_fk;
       public          postgres    false    3008    211    231            �           2606    75448    deviation deviation_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_fk FOREIGN KEY (plant_parameter) REFERENCES public.plant_parameter(id) ON UPDATE CASCADE ON DELETE CASCADE;
 @   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_fk;
       public          postgres    false    219    2990    211            �           2606    75453    deviation deviation_plant_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_plant_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_plant_fk;
       public          postgres    false    218    2986    211            �           2606    75458    deviation deviation_product_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_product_fk FOREIGN KEY (product) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE CASCADE;
 H   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_product_fk;
       public          postgres    false    211    2992    221            �           2606    75463    deviation deviation_size_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.deviation
    ADD CONSTRAINT deviation_size_fk FOREIGN KEY (size) REFERENCES public.size(id) ON UPDATE CASCADE ON DELETE CASCADE;
 E   ALTER TABLE ONLY public.deviation DROP CONSTRAINT deviation_size_fk;
       public          postgres    false    224    2996    211            �           2606    75468    machine machine_fk    FK CONSTRAINT     w   ALTER TABLE ONLY public.machine
    ADD CONSTRAINT machine_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name);
 <   ALTER TABLE ONLY public.machine DROP CONSTRAINT machine_fk;
       public          postgres    false    213    2986    218            �           2606    75473    ncr_data ncr_data_approvedby_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_approvedby_fk FOREIGN KEY (qa_representative) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 I   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_approvedby_fk;
       public          postgres    false    231    3008    214            �           2606    75478    ncr_data ncr_data_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_fk FOREIGN KEY (qa_representative) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_fk;
       public          postgres    false    214    3008    231            �           2606    82861    ncr_data ncr_data_plant_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_plant_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name) ON UPDATE CASCADE ON DELETE CASCADE;
 D   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_plant_fk;
       public          postgres    false    218    2986    214            �           2606    82866    ncr_data ncr_data_product_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_product_fk FOREIGN KEY (product) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE CASCADE;
 F   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_product_fk;
       public          postgres    false    2992    214    221            �           2606    82872    ncr_data ncr_data_size_fk    FK CONSTRAINT     t   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_size_fk FOREIGN KEY (size) REFERENCES public.size(id);
 C   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_size_fk;
       public          postgres    false    2996    214    224            �           2606    75483    ncr_data ncr_data_user_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.ncr_data
    ADD CONSTRAINT ncr_data_user_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 C   ALTER TABLE ONLY public.ncr_data DROP CONSTRAINT ncr_data_user_fk;
       public          postgres    false    214    3008    231            �           2606    75488 #   plant_parameter plant_parameters_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_parameters_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name);
 M   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_parameters_fk;
       public          postgres    false    219    218    2986            �           2606    75493 (   plant_parameter plant_parameters_size_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_parameters_size_fk FOREIGN KEY (unit) REFERENCES public.unit(unit) ON UPDATE CASCADE ON DELETE RESTRICT;
 R   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_parameters_size_fk;
       public          postgres    false    230    219    3006            �           2606    75498    plant_parameter plant_params_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.plant_parameter
    ADD CONSTRAINT plant_params_fk FOREIGN KEY (parameter) REFERENCES public.parameter(name);
 I   ALTER TABLE ONLY public.plant_parameter DROP CONSTRAINT plant_params_fk;
       public          postgres    false    216    219    2980            �           2606    75503    product product_fk    FK CONSTRAINT     w   ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name);
 <   ALTER TABLE ONLY public.product DROP CONSTRAINT product_fk;
       public          postgres    false    218    2986    221            �           2606    75508    size size_fk    FK CONSTRAINT     m   ALTER TABLE ONLY public.size
    ADD CONSTRAINT size_fk FOREIGN KEY (product) REFERENCES public.product(id);
 6   ALTER TABLE ONLY public.size DROP CONSTRAINT size_fk;
       public          postgres    false    2992    221    224            �           2606    75513 =   size_spec_deviations size_spec_deviations_fk_plant_parameters    FK CONSTRAINT     �   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_fk_plant_parameters FOREIGN KEY (plant_parameter) REFERENCES public.plant_parameter(id) ON UPDATE CASCADE ON DELETE CASCADE;
 g   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_fk_plant_parameters;
       public          postgres    false    2990    219    226            �           2606    75518 4   size_spec_deviations size_spec_deviations_fk_product    FK CONSTRAINT     �   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_fk_product FOREIGN KEY (product) REFERENCES public.product(id);
 ^   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_fk_product;
       public          postgres    false    226    221    2992            �           2606    75523 1   size_spec_deviations size_spec_deviations_fk_size    FK CONSTRAINT     �   ALTER TABLE ONLY public.size_spec_deviations
    ADD CONSTRAINT size_spec_deviations_fk_size FOREIGN KEY (size) REFERENCES public.size(id) ON UPDATE CASCADE ON DELETE CASCADE;
 [   ALTER TABLE ONLY public.size_spec_deviations DROP CONSTRAINT size_spec_deviations_fk_size;
       public          postgres    false    224    2996    226            �           2606    75528    parameter spec_parameter_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.parameter
    ADD CONSTRAINT spec_parameter_fk FOREIGN KEY (parameter_type) REFERENCES public.parameter_type(type_name);
 E   ALTER TABLE ONLY public.parameter DROP CONSTRAINT spec_parameter_fk;
       public          postgres    false    216    2984    217            �           2606    75533    specification specification_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_fk FOREIGN KEY (plant_parameter) REFERENCES public.plant_parameter(id);
 H   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_fk;
       public          postgres    false    228    2990    219            �           2606    75538 &   specification specification_product_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_product_fk FOREIGN KEY (product) REFERENCES public.product(id);
 P   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_product_fk;
       public          postgres    false    228    2992    221            �           2606    75543 #   specification specification_size_fk    FK CONSTRAINT     ~   ALTER TABLE ONLY public.specification
    ADD CONSTRAINT specification_size_fk FOREIGN KEY (size) REFERENCES public.size(id);
 M   ALTER TABLE ONLY public.specification DROP CONSTRAINT specification_size_fk;
       public          postgres    false    224    228    2996            �           2606    75548    user user_department_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_department_fk FOREIGN KEY (department) REFERENCES public.department(name) ON UPDATE CASCADE ON DELETE RESTRICT;
 C   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_department_fk;
       public          postgres    false    2974    231    210            �           2606    75553    user user_fk    FK CONSTRAINT     p   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_fk FOREIGN KEY (role) REFERENCES public.user_role(role);
 8   ALTER TABLE ONLY public."user" DROP CONSTRAINT user_fk;
       public          postgres    false    231    3016    235            �           2606    75558    user_log user_log_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_log
    ADD CONSTRAINT user_log_fk FOREIGN KEY (created_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.user_log DROP CONSTRAINT user_log_fk;
       public          postgres    false    231    233    3008            �           2606    75563    user_log user_log_fk_shift    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_log
    ADD CONSTRAINT user_log_fk_shift FOREIGN KEY (shift) REFERENCES public.shift(name) ON UPDATE RESTRICT ON DELETE RESTRICT;
 D   ALTER TABLE ONLY public.user_log DROP CONSTRAINT user_log_fk_shift;
       public          postgres    false    233    223    2994            �           2606    75568    user_log user_log_plant_fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_log
    ADD CONSTRAINT user_log_plant_fk FOREIGN KEY (plant) REFERENCES public.plant(plant_name) ON UPDATE RESTRICT ON DELETE RESTRICT;
 D   ALTER TABLE ONLY public.user_log DROP CONSTRAINT user_log_plant_fk;
       public          postgres    false    233    218    2986            �           2606    99285 :   wafer_sticks_packaging wafer_sticks_packaging_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.wafer_sticks_packaging
    ADD CONSTRAINT wafer_sticks_packaging_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 d   ALTER TABLE ONLY public.wafer_sticks_packaging DROP CONSTRAINT wafer_sticks_packaging_product_fkey;
       public          postgres    false    221    2992    237            �           2606    99290 8   wafer_sticks_packaging wafer_sticks_packaging_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.wafer_sticks_packaging
    ADD CONSTRAINT wafer_sticks_packaging_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 b   ALTER TABLE ONLY public.wafer_sticks_packaging DROP CONSTRAINT wafer_sticks_packaging_shift_fkey;
       public          postgres    false    2994    237    223            �           2606    99280 7   wafer_sticks_packaging wafer_sticks_packaging_size_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.wafer_sticks_packaging
    ADD CONSTRAINT wafer_sticks_packaging_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 a   ALTER TABLE ONLY public.wafer_sticks_packaging DROP CONSTRAINT wafer_sticks_packaging_size_fkey;
       public          postgres    false    224    237    2996            �           2606    99275 :   wafer_sticks_packaging wafer_sticks_packaging_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.wafer_sticks_packaging
    ADD CONSTRAINT wafer_sticks_packaging_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 d   ALTER TABLE ONLY public.wafer_sticks_packaging DROP CONSTRAINT wafer_sticks_packaging_user_id_fkey;
       public          postgres    false    237    3008    231                        2606    99318 6   wafer_sticks_process wafer_sticks_process_product_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.wafer_sticks_process
    ADD CONSTRAINT wafer_sticks_process_product_fkey FOREIGN KEY (product) REFERENCES public.product(id);
 `   ALTER TABLE ONLY public.wafer_sticks_process DROP CONSTRAINT wafer_sticks_process_product_fkey;
       public          postgres    false    239    2992    221                       2606    99323 4   wafer_sticks_process wafer_sticks_process_shift_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.wafer_sticks_process
    ADD CONSTRAINT wafer_sticks_process_shift_fkey FOREIGN KEY (shift) REFERENCES public.shift(name);
 ^   ALTER TABLE ONLY public.wafer_sticks_process DROP CONSTRAINT wafer_sticks_process_shift_fkey;
       public          postgres    false    223    239    2994            �           2606    99313 3   wafer_sticks_process wafer_sticks_process_size_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.wafer_sticks_process
    ADD CONSTRAINT wafer_sticks_process_size_fkey FOREIGN KEY (size) REFERENCES public.size(id);
 ]   ALTER TABLE ONLY public.wafer_sticks_process DROP CONSTRAINT wafer_sticks_process_size_fkey;
       public          postgres    false    239    2996    224            �           2606    99308 6   wafer_sticks_process wafer_sticks_process_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.wafer_sticks_process
    ADD CONSTRAINT wafer_sticks_process_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id);
 `   ALTER TABLE ONLY public.wafer_sticks_process DROP CONSTRAINT wafer_sticks_process_user_id_fkey;
       public          postgres    false    231    239    3008            �   q   x��̻	�0 ��2EH����d'HPQ�4������s@� 1��4�!���dP�&�����6�J����E�W�����{�5/��}�ߞ�.�_ε��*�.�4      �   F  x���[n�0�o�
6 ��	,�+�Qp	��d�%��I#���#ϝ1�@@�-ʝ(�ں֍�9��B@h@���(!9�"�6o���X��/'O~=�yx�*����W˜>�(�>�^8gl�q�m�Um��M��q�Wi�qY�{�{S�i�O ��� �l�љ+�f��.�N�C���`Z�*�Y|U�f?�������X@�<����٦x��ƚ1ڊ\��-�:J��;ӝ�QK=Y��5�����_�\׾�\ �s����袗��w��t+�'�������?
$��M2�[�[^����T7�r��`�xڛ�6��y��^      �      x������ � �      �      x������ � �      �   I   x�u����,M��,�Tp,..-J�KNUpI-H,*�M�+�
�w	u����(�O)M.���CV���� ڈ<      �   B   x�3�445�44�12�44�4202�50�52T04�22�26�41�47�t�,N.�,*������ H�7      �      x������ � �      �   C   x�3�44���C ##C]]#S+Sc+#sN������NC#���� g��`�rs�=... /�U      �   �   x����
�0������9�bwˆD�Q]2�WG5c�z��"�I��;�h:�fi�<A�m��YD	�\B]:v�ψ�B���jmg %��LlжV���m��kPUm�c[����6��<^$� _�F�����k�_;��JyzM�i�y&��.�c$d!Ž����C6�7e�ߌ:���ŁG���k����#��m�m      �   �   x�M��
�0E��+�t�.� �� ��X�I IK�ϷUK{7�p�[��܄�y
�i��B��y)V@[
��,+����L�O�k�b����ۢX�M�v�ۚ�v�N���O�E��.�'yx+�(P8U4`��cT#�
�H<:I0��z�b�+?-�h����{lc��R�      �   �   x�uλ�  ���? /�(c�w�������Bx���]�v>˹����(���b��MԊI%��}��oX�8���{B�rIQ�D��e2ċh��T,���LK�&z�wL2!8�<��g���q vq��X�b��$�� +�߸TM�j��1�|�:�      �   �   x��λ�0 ��}
G�W�n���lB�Rb��}}������?�2p2A%�k]� 5�.)IU*��e9:b�zu���wc{�C�����|輳�y���ew\��e�����rY��I5�Q�YIX��@���}^��fв�~e@�f���ፚ�װ�}��'M�m!|��_6      �   �   x��н�� @����ȕ�W�b�}�mXŨ0p!��'�&��Τ<�7gWM��J�
���L$�"3�=�^�B�6*��nT�Ut��ML�F�Z�hb5� Mל��VE��~�Vv��͋ō�]�V,n"P;�p�:��/�}Ų�	2$f���k�F�'���i*��,��,;L���{�Z�/��O���x�R�Mٵ��2��Ӝ���I�nǐ��|Y��'�      �   "   x�+��L+�5��"�b0��g���qqq I�p      �   �   x�}λ
�@������Pv���
�"�ڬ�F11�&E����
�14mc�U!�d��@���
Z\�<ԗ��C<��Mj�&ԑ`ߙ����M�5�':}"�c�)�PU�
��=���W�-�9FA��>4��lNa,�3&4�ˑ�^��#�^<�hZ�T�_b��!3? 8�@      �   r   x�}�A�0��5��t�Z[�����3et�ф��#��F_�����m�ZHQ��)�6o��F�Sԫ�3����i$�dB���)�}��w��';�g�0����?�+)�      �   �   x�m���0�3Eh���!��W ��q҈J���ӧ�^b�"Ex02�(3.��,�.�K�����M�Ya{�3oG�����<A����.#�j�E9�f��$�]u���Ȁx�����uts�L�^l�ձ*�\mԙ5���+��R���>�      �   }   x�5�1�0Eg�YX��s#�̒�&�;�� q{��Oz�Y �v�d��s�c�)Yo��C��=��/^G�a(wG���@�$�[��k�A�\A[i�+�T��Ⱦ�xw�Ì4��qF��01z      �   �  x����n�@���Sd�{f�o�p����1EB�g��c�����7�d�v�V:�#��O?-(9%�u�g�|�y�my�!&C�Ÿ~���������XZ��0�u��0��Fs��T�%�fo�Sڒ�sR2
�jRLCRҞ
F
�Ȥa����MSɉ�
&�0�-MJ"z%�Jۜ�c5)H�Զ�9��H��n�Z	~Nrv�b�p&��Nux�����\I�`���6�6���'e&"���#��7�lqt�%p�������w,H������z���ղ%"%���|g���Z4}�F�I�hfюy�|-Ȱu�̛�Mr$ɯ}�	6l�!���y!Q����j&���o�?"��|t����x�
�,�.��5o��P�����}�U��s�	�:�"_y� ���aa̺�N�	���ו�A+oi�Dwp<��D�%b���af��K�=��SYXZ��D���SzS��2"      �   b   x��;
�0 �9=E.PICAq�
:������z}uy�*Må����|~���e�^ �:�g��U�d=�����(�v������S�\`e�K&�fι���      �   �   x�m��
�0��cw^A�0t��Q�˜1�	ۻȻψD���ǟGE�[\RB��I�8Zm|�h/��g�&�����ü;��k�����>5AKz\k������n[�Z�]�rYn�Y�)��d��g�4��e�����!��K���!�i1M�      �      x������ � �      �      x������ � �     